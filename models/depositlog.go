package models

import (
	"time"
)

type Receipt struct {
	passthrough string
	Payload     Payload
}

type Payload struct {
	OrderID       string
	PackageName   string
	ProductID     string
	PurchaseToken string
}

type GatewayDepositLog struct {
	ID                int64
	Gateway           string
	External_id       string
	Transaction_id    string
	User_id           string
	Account_name      string
	Package_id        string
	Amount            float64 `json:"amount" sql:"type:decimal(25,10)"`
	currency_id       string
	created_at        int64
	Status            int
	Bundle_content    string
	Memo              string
	refund_prediction int
	Refunded          int

}

type GatewayDepositLogRefund struct {
	ID int64
	DepositlogID     int64
	ExternalID       string
	TransactionID    string
	UserId           string
	BundleAmount     int64
	UserBalance      int64
	Gateway          string
	createdAt       time.Time
}

func (t GatewayDepositLog) TableName() string {
	return "gateway_depositlog_new"
}

func (t GatewayDepositLogRefund) TableName() string{
	return "gateway_depositlog_refund"
}

func Find48agoDepositLogs() ([]*GatewayDepositLog, error) {
	var depositLogs []*GatewayDepositLog

	err := DBReadonly.Where("refunded=? or refunded is null", "0").Where("created_at >= ?", time.Now().Unix() - 48*60*60).Where("gateway=? and status=2", "googleplayiab").Order("id").Find(&depositLogs).Error
	if err != nil {
		return nil, err
	}

	return depositLogs, nil
}

func FindDepositLogWithExternalId(externalId string) (*GatewayDepositLog, error) {
	var depositLog GatewayDepositLog

	err := DBReadonly.Where("external_id=?", externalId).Where("created_at >= ?", time.Now().Unix() - 30*24*60*60).First(&depositLog).Error

	return &depositLog, err
}

func FindDepositLogWithExternalIdInYear(externalId string) (*GatewayDepositLog, error) {
	var depositLog GatewayDepositLog

	err := DBReadonly.Where("external_id=?", externalId).Where("created_at >= ?", time.Now().Unix() - 12*30*24*60*60).First(&depositLog).Error

	return &depositLog, err
}

func UpdateDepositLogRefunded(id int64) error {
	depositLog := &GatewayDepositLog{}
	err:= DB.Model(depositLog).Where("id=?",id).UpdateColumn("refunded", 1).Error
	if err!= nil {
		return err
	}

	return nil
}

func CreateDepositLogRefund(userId string, gateway string, deposiglogId int64, externalId string, transactionId string, bundleAmount int64, userBalance int64) error {
	depositLogRefund := &GatewayDepositLogRefund{UserId: userId, Gateway: gateway, DepositlogID: deposiglogId, ExternalID:externalId, TransactionID: transactionId, BundleAmount:bundleAmount, UserBalance: userBalance}

	err := DB.Create(depositLogRefund).Error

	return err
}
