package models

type FifoConsumed struct {
	ID            int
	TransactionID string
	ExternalID    string
	UserID        string
	Value         float64
	Memo          string
	Status        int
	CreatedAt     int64
	Details       string
}

func (fc FifoConsumed) TableName() string {
	return "fifo_fifoconsumed"
}

type FifoAccount struct {
	ID         int
	UserID     string
	Name       string
	CurrencyId string
	Amount     int64
	Value      float64
	QueueType  int
	CreatedAt  int64
	SourceType string
}

func (fa FifoAccount) TableName() string {
	return "fifo_fifoaccount"
}
