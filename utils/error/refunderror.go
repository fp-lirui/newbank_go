package error

type ErrorCode int

const (
	INTERNAL_ERROR ErrorCode = 1 + iota
	NOT_EXIST_ENTIES_ERROR
	INVALID_PARAMETER_ERROR
)


type RefundError struct {
	Code int
	Message string
}

var errorCodes = [...]RefundError{
	RefundError{Code: -1, Message:"内部错误"},
	RefundError{Code: 1000, Message:"没有记录"},
	RefundError{Code: 1001, Message:"参数错误"},
}

func (e ErrorCode) Code() int {
	return errorCodes[e - 1].Code
}

func (e ErrorCode) Error() string {
	return errorCodes[e - 1].Message
}
