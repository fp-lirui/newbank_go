package service

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	log "github.com/sirupsen/logrus"
	"st/config"
	"st/models"
	"strings"
)

func MakeRedshfitConnection(username, password, host, port, dbName string) (*sql.DB, error) {

	url := fmt.Sprintf("sslmode=require user=%v password=%v host=%v port=%v dbname=%v",
		username,
		password,
		host,
		port,
		dbName)

	var err error
	var db *sql.DB
	if db, err = sql.Open("postgres", url); err != nil {
		return nil, fmt.Errorf("redshift connect error : (%v)", err)
	}

	if err = db.Ping(); err != nil {
		return nil, fmt.Errorf("redshift ping error : (%v)", err)
	}
	return db, nil
}

func Test() error {
	username := config.Config.Redshift.Username
	password := config.Config.Redshift.Password
	host := config.Config.Redshift.Host
	port := config.Config.Redshift.Port
	dbName := config.Config.Redshift.DbName
	log.WithFields(log.Fields{"host": host, "dbName": dbName}).Info("connect to redshfit.")
	db, err := MakeRedshfitConnection(username, password, host, port, dbName)

	if err != nil {
		log.Error("Connect Redshift Err:", err)
	} else {
		day := `2020-03-01`
		q := fmt.Sprintf("select * from meme.funbank_meme_deposit where date(ts) = '%s' LIMIT 2;", day)
		rows, err := db.Query(q)
		if err != nil {
			log.Error("Query Err:", err)
		}
		defer rows.Close()
		for rows.Next() {
			var (
				trans_id  string
				exter_id  string
				gateway   string
				uid       string
				account   string
				amount    string
				currency_id  string
				ts         string
				mcoin      string
				package_id string
				usd        string
				country    string
			)
			if err := rows.Scan(&trans_id, &exter_id, &gateway, &uid, &account, &amount, &currency_id, &ts, &mcoin, &package_id, &usd, &country); err != nil {
				log.Error("row Scan Err:", err)
			}
			log.Printf("trans_id %s \t exter_id %s \t gateway %s \t uid %s \t account %s \t \n", trans_id, exter_id,
				gateway, uid, account)
		}
	}

	return nil
}

func SyncGoogleVoidedPurchasesTable() error {
	voidedPurchases, err := models.FindGoogleVoidedPurchases()
	if err != nil {
		log.WithFields(log.Fields{"err": err}).Error("FindGoogleVoidedPurchases")
		return err
	}

	if len(voidedPurchases) == 0 {
		log.Error("FindGoogleVoidedPurchases return 0.")
		return nil
	}

	purchaseList := make([]string, len(voidedPurchases))
	for _, value := range voidedPurchases {
		sql := fmt.Sprintf("('%s', '%s', %d, %d, %d, %d, %d, %d)", value.OrderId, value.PurchaseToken, value.VoidedReason, value.VoidedSource,
			value.VoidedTimeMillis, value.PurchaseTimeMillis, value.Status, value.CreatedAt)
		purchaseList = append(purchaseList, sql)
	}

	if len(purchaseList) == 0 {
		log.Error("FindGoogleVoidedPurchases return 0.")
		return nil
	}
	log.WithFields(log.Fields{"purchaseList": purchaseList}).Info("FindGoogleVoidedPurchases.")

	username := config.Config.Redshift.Username
	password := config.Config.Redshift.Password
	host := config.Config.Redshift.Host
	port := config.Config.Redshift.Port
	dbName := config.Config.Redshift.DbName
	log.WithFields(log.Fields{"host": host, "dbName": dbName}).Info("connect to redshfit.")
	db, err := MakeRedshfitConnection(username, password, host, port, dbName)

	defer db.Close()

	if err != nil {
		log.Error("Connect Redshift Err:", err)
		return err
	} else {
		result, err := db.Exec("SET search_path TO 'newbank';SET statement_timeout = 1800000;")
		if err != nil {
			log.Error("db Exec(SET search_path TO 'newbank';SET statement_timeout = 1800000) Err:", err)
			return err
		} else {
			log.WithFields(log.Fields{"result": result}).Info("db Exec(SET search_path TO 'newbank';SET statement_timeout = 1800000);")
		}
		result, err = db.Exec("CREATE TEMP TABLE gateway_googleplay_voidedpurchase_stage (LIKE gateway_googleplay_voidedpurchase)")
		if err != nil {
			log.Error("db.Exec(CREATE TEMP TABLE gateway_googleplay_voidedpurchase_stage (LIKE gateway_googleplay_voidedpurchase) Err:", err)
			return err
		} else {
			log.WithFields(log.Fields{"result": result}).Info("db.Exec(CREATE TEMP TABLE gateway_googleplay_voidedpurchase_stage (LIKE gateway_googleplay_voidedpurchase);")
		}
		var sqlVal string
		for _, values := range purchaseList {
			sqlVal += values
			sqlVal += ","
		}
		if last := len(sqlVal) - 1; last >= 0 && sqlVal[last] == ',' {
			sqlVal = sqlVal[:last]
		}
		log.WithFields(log.Fields{"sqlVal": sqlVal}).Info("db.Exec()")
		sqlVal = strings.TrimLeft(sqlVal, ",")
		log.WithFields(log.Fields{"sqlVal": sqlVal}).Info("db.Exec()")
		tmpSql := fmt.Sprintf("INSERT INTO gateway_googleplay_voidedpurchase_stage(order_id, purchase_token, voided_reason, voided_source, voided_time_millis, purchase_time_millis, status, created_at) VALUES %s", sqlVal)
		log.WithFields(log.Fields{"tmpSql": tmpSql}).Info("db.Exec()")

		result, err = db.Exec(tmpSql)
		if err != nil {
			log.Error("db.Exec(tmpSql) Err:", err)
			return err
		}
		log.WithFields(log.Fields{"tmpSql": tmpSql}).Info("db.Exec()")

		result, err = db.Exec(`
				BEGIN TRANSACTION;
				DELETE FROM gateway_googleplay_voidedpurchase USING gateway_googleplay_voidedpurchase_stage WHERE gateway_googleplay_voidedpurchase.order_id = gateway_googleplay_voidedpurchase_stage.order_id;
				INSERT INTO gateway_googleplay_voidedpurchase(order_id, purchase_token, voided_reason, voided_source, voided_time_millis, purchase_time_millis, status, created_at)
				SELECT a.order_id, a.purchase_token, a.voided_reason, a.voided_source, a.voided_time_millis, a.purchase_time_millis, a.status, a.created_at FROM gateway_googleplay_voidedpurchase_stage AS a;
				END TRANSACTION;`)

		if err != nil {
			log.Error("db.Exec(Begin Transaction; ... END Transaction;) Err:", err)
			return err
		}
	}
	return nil
}