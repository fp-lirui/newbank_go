package models

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"st/config"
	"strconv"
	"time"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

type WalletDiamond struct {
	// gorm.Model
	ID        int    `gorm:"primary_key"`
	UserId    string `gorm:"type:varchar(50);not null;index:ip_idx"`
	Sn        string `gorm:"type:varchar(50);default:'S0';"`
	Account   string `gorm:"type:varchar(256);not null;"`
	Num       int64
	CreatedAt string
}

func (t WalletDiamond) TableName() string {
	tableIndex, err := strconv.ParseInt(t.UserId, 10, 64)
	if err != nil {
		return "newbank_walletdiamond_e1"
	}

	return "newbank_walletdiamond_" + strconv.Itoa(int(tableIndex)%config.Config.Dbbak.Tablesplice)
}

func AddWalletDiamond(user_id string, sn string, account string, num int64) error {
	walletDiamons := WalletDiamond{UserId: user_id, Sn: sn, Account: account, Num: num, CreatedAt: time.Now().Format("2006-01-02 03:04:05.999999999")}

	if err := DB.Table((&WalletDiamond{UserId: user_id}).TableName()).Create(&walletDiamons).Error; err != nil {
		return err
	}

	return nil
}

func FindUserWalletDiamond(user_id string, account string) ([]*WalletDiamond, error) {
	var walletDiamond []*WalletDiamond

	err := DB.Table((&WalletDiamond{UserId: user_id}).TableName()).Where(&WalletDiamond{UserId: user_id, Account: account}).Order("id asc").Find(&walletDiamond).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		fmt.Println("FindUserWallet() ErrRecordNotFound")
		return nil, err
	}

	return walletDiamond, nil
}

func DeleteUserWalletDiamond(id int, user_id string, account string) error {
	if err := DB.Table((&WalletDiamond{UserId: user_id}).TableName()).Where("id <= ?", id).Where("user_id = ? ", user_id).Where("account = ?", account).Delete(&WalletDiamond{}).Error; err != nil {
		return err
	}

	return nil
}

func UpdateUserWalletDiamond(id int, user_id string, num int64) error {
	walletDiamond := WalletDiamond{Num: num}
	if err := DB.Table((&WalletDiamond{UserId: user_id}).TableName()).Model(&WalletDiamond{}).Where("id = ? ", id).Update(&walletDiamond).Error; err != nil {
		return err
	}

	return nil
}

type WalletTicket struct {
	// gorm.Model
	ID        int    `gorm:"primary_key"`
	UserId    string `gorm:"type:varchar(50);not null;index:ip_idx"`
	Sn        string `gorm:"type:varchar(50);default:'S0';"`
	Account   string `gorm:"type:varchar(256);not null;"`
	Num       int64
	CreatedAt string
}

func (t WalletTicket) TableName() string {
	tableIndex, err := strconv.ParseInt(t.UserId, 10, 64)
	if err != nil {
		return "newbank_walletticket_e1"
	}

	return "newbank_walletticket_" + strconv.Itoa(int(tableIndex)%config.Config.Dbbak.Tablesplice)
}

func AddWalletTicket(user_id string, sn string, account string, num int64) error {
	walletTicket := WalletTicket{UserId: user_id, Sn: sn, Account: account, Num: num, CreatedAt: time.Now().Format("2006-01-02 03:04:05.999999999")}

	if err := DB.Table((&WalletTicket{UserId: user_id}).TableName()).Create(&walletTicket).Error; err != nil {
		return err
	}

	return nil
}

func FindUserWalletTicket(user_id string, account string) ([]*WalletTicket, error) {
	var walletTicket []*WalletTicket

	err := DB.Table((&WalletTicket{UserId: user_id}).TableName()).Where(&WalletTicket{UserId: user_id, Account: account}).Order("id asc").Find(&walletTicket).Error
	if err != nil {
		fmt.Println("FindUserWallet() ErrRecordNotFound")
		return nil, err
	}

	return walletTicket, nil
}

func DeleteUserWalletTicket(id int, user_id string, account string) error {

	if err := DB.Table((&WalletTicket{UserId: user_id}).TableName()).Where("id <= ?", id).Where("user_id = ?", user_id).Where("account = ?", account).Delete(&WalletTicket{}).Error; err != nil {
		return err
	}

	return nil
}

func UpdateUserWalletTicket(id int, user_id string, num int64) error {
	walletTicket := WalletTicket{Num: num}
	if err := DB.Table((&WalletTicket{UserId: user_id}).TableName()).Model(&WalletTicket{}).Where("id = ? ", id).Updates(&walletTicket).Error; err != nil {
		return err
	}

	return nil
}

type WalletDataConfig struct {
	ID    int    `gorm:primary_key`
	Param string `gorm:"type:varchar(20);not null;index:ip_idx"`
	Value string `gorm:"type:varchar(20);not null"`
}

func (WalletDataConfig) TableName() string {
	return "newbank_walletdataconfig"
}

func GetWalletDataConfig(maps interface{}) (int, error) {
	var walletDataConfig WalletDataConfig
	if err := DB.Where(maps).First(&walletDataConfig).Error; err != nil {
		return 0, err
	}
	return strconv.Atoi(walletDataConfig.Value)
}

func UpdateWalletDataConfigCursor(cursor int64) error {
	updates := WalletDataConfig{Param: "cursor"}
	if err := DB.Model(&WalletDataConfig{}).Where(&updates).Update("value", strconv.FormatInt(cursor, 10)).Error; err != nil {
		return err
	}
	return nil
}

type CoreAccount struct {
	// gorm.Model
	ID       int    `gorm:"primary_key"`
	UserId   string `gorm:"type:varchar(50);not null;index:ip_idx"`
	Name     string `gorm:"type:varchar(50);default:'S0';"`
	Currency string `gorm:"type:varchar(50);not null;"`
	Balance  int64
}

func (CoreAccount) TableName() string {
	return "newbank_coreaccount"
}

func GetCoreAccount(user_id string, account string, currency string) (CoreAccount, error) {
	var coreAccount CoreAccount

	if err := DB.Where(&CoreAccount{UserId: user_id, Name: account, Currency: currency}).First(&coreAccount).Error; err != nil {
		return coreAccount, err
	}

	return coreAccount, nil
}

func UpdateCoreAccount(user_id string, account string, currency string, balance int64) error {
	updates := CoreAccount{UserId: user_id, Name: account, Currency: currency}
	fmt.Println("updates :", updates)
	if err := DB.Model(&updates).Where(&updates).Update("balance", balance).Error; err != nil {
		return err
	}
	return nil
}

func UpdateCoreAccountBalancePlus(user_id string, account string, currency string, balance int64) error {
	updates := CoreAccount{UserId: user_id, Name: account, Currency: currency}
	if err := DB.Model(&updates).Where(&updates).UpdateColumn("balance", gorm.Expr("balance + ?", balance)).Error; err != nil {
		if gorm.IsRecordNotFoundError(err) {
			if err1 := DB.Create(&CoreAccount{UserId: user_id, Name: account, Currency: currency, Balance: balance}).Error; err1 != nil {
				return err1
			}
			return nil
		}
		return err
	}
	return nil
}

func UpdateCoreAccountBalanceMinus(user_id string, account string, currency string, balance int64) error {
	updates := CoreAccount{UserId: user_id, Name: account, Currency: currency}
	if err := DB.Model(&updates).Where(&updates).UpdateColumn("balance", gorm.Expr("balance - ?", balance)).Error; err != nil {
		return err
	}
	return nil
}

type SnConfig struct {
	// gorm.Model
	ID        int     `gorm:"primary_key"`
	Sn        string  `gorm:"type:varchar(50);not null;default:'S0';unique;index:sn_idx"`
	Channel   string  `gorm:"type:varchar(250);"`
	PackageId string  `gorm:"type:varchar(250);`
	Amount    float64 `gorm:type:decimal(25,10);`
	Bundle    int64   `gorm:type:varchar(50);`
	Value     float64 `gorm:type:decimal(25,10;`
	Currency  string  `gorm:"type:varchar(50)"`
}

func (SnConfig) TableName() string {
	return "newbank_snconfig"
}

func AddSnConfig(transactionId string) (SnConfig, error) {
	fmt.Println("Addsnconfig 1:", transactionId)
	depositLog := Depositlog{TransactionId: transactionId}
	var snConfig SnConfig
	if err := DB.Model(&Depositlog{}).Where("transaction_id = ?", transactionId).First(&depositLog).Error; err != nil {
		return snConfig, err
	}
	fmt.Println("Addsnconfig depositLog:", depositLog)
	// 计算单币价值 单位USD
	usdrate, _ := GetUsdRate(depositLog.CurrencyId, "Default")
	fmt.Println("Addsnconfig usdrate:", usdrate)
	var bundleStr = depositLog.BundleContent
	bundleData := []byte(bundleStr)
	var bundleJson map[string]string
	if err1 := json.Unmarshal(bundleData, &bundleJson); err1 != nil {
		fmt.Println("AddSnConfig() json.Unmarshal err1:", err1)
		return snConfig, err1
	}
	fmt.Println("Addsnconfig bundleJson:", bundleJson)
	diamond, err2 := strconv.ParseInt(bundleJson["DIAMOND"], 10, 64)
	if err2 != nil {
		fmt.Println("AddSnConfig() strconv.ParseFloat err2:", err2)
		return snConfig, err2
	}
	fmt.Println("Addsnconfig diamond:", diamond)
	var value = (depositLog.Amount / usdrate) / float64(diamond)
	fmt.Println("Addsnconfig value:", value)
	// 生成唯一sn
	var sn string
	for {
		sn = RandStringRunes(3)
		var count int
		if err3 := DB.Model(&SnConfig{}).Where(&SnConfig{Sn: sn}).Count(&count).Error; err3 != nil {
			fmt.Println("AddSnConfig() findSn err:", err3)
			return snConfig, err3
		}
		if count > 0 {
			continue
		} else {
			break
		}
	}

	snConfig = SnConfig{Sn: sn, Channel: depositLog.Gateway, PackageId: depositLog.PackageId, Amount: depositLog.Amount, Bundle: diamond, Currency: depositLog.CurrencyId, Value: value}

	if err := DB.Create(&snConfig).Error; err != nil {
		return snConfig, err
	}

	return snConfig, nil
}

func AddSnConfigWithValue(gateway string, packageid string, amount float64, diamond int64, currency string, value float64) (SnConfig, error) {
	var snConfig SnConfig
	// 生成唯一sn
	var sn string

	var count int
	if err3 := DB.Model(&SnConfig{}).Count(&count).Error; err3 != nil {
		fmt.Println("AddSnConfig() findSn1 err:", err3)
		return snConfig, err3
	}
	sn = genSn(count)

	// for {

	// 	var count int
	// 	if err3 := DB.Model(&SnConfig{}).Count(&count).Error; err3 != nil {
	// 		fmt.Println("AddSnConfig() findSn1 err:", err3)
	// 		return snConfig, err3
	// 	}
	// 	sn = genSn(count)
	// 	if err3 := DB.Model(&SnConfig{}).Where(&SnConfig{Sn: sn}).Count(&count).Error; err3 != nil {
	// 		fmt.Println("AddSnConfig() findSn2 err:", err3)
	// 		return snConfig, err3
	// 	}
	// 	if count > 0 {
	// 		continue
	// 	} else {
	// 		break
	// 	}
	// }

	snConfig = SnConfig{Sn: sn, Channel: gateway, PackageId: packageid, Amount: amount, Bundle: diamond, Currency: currency, Value: value / amount}

	if err := DB.Create(&snConfig).Error; err != nil {
		return snConfig, err
	}

	return snConfig, nil
}

func FindSn(channel string, package_id string) (SnConfig, error) {
	var snConfig SnConfig

	if err := DB.Where("channel = ?", channel).Where("package_id = ?", package_id).First(&snConfig).Error; err != nil {
		return snConfig, err
	}

	return snConfig, nil
}

type Depositlog struct {
	// gorm.Model
	ID               int     `gorm:"primary_key"`
	Gateway          string  `gorm:"type:varchar(250);not null;index:sn_idx"`
	ExternalId       string  `gorm:"type:varchar(250);unique_index"`
	TransactionId    string  `gorm:"type:varchar(250);unique_index`
	UserId           string  `gorm:"type:varchar(250);"`
	AccountName      string  `gorm:"type:varchar(250);"`
	PackageId        string  `gorm:"type:varchar(250);"`
	Amount           float64 `gorm:"type:decimal(25,10);"`
	CurrencyId       string  `gorm:"type:varchar(10);"`
	CreatedAt        int64   `gorm:"type:bigint(20);"`
	Status           int     `gorm:"type:smallint(6)"`
	BundleContent    string  `gorm:"type:longtext;"`
	Memo             string  `gorm:"type:longtext;"`
	Passthrough      string  `gorm:"type:longtext;"`
	RefundPrediction int     `gorm:"type:smallint(6)"`
	Refunded         int     `gorm:"type:smallint(6)"`
}

func (Depositlog) TableName() string {
	return "gateway_depositlog_new"
}

func FindDepositLogs(transaction_id string) (Depositlog, error) {
	depositLog := Depositlog{TransactionId: transaction_id}
	if err := DB.Where("transaction_id = ?", transaction_id).First(&depositLog).Error; err != nil {
		return depositLog, err
	}

	return depositLog, nil
}

type ExchangeRate struct {
	ID                int
	Rate              float64
	BaseCurrencyId    string
	CounterCurrencyId string
	Region            string
}

func (ExchangeRate) TableName() string {
	return "core_exchangerate"
}

func GetUsdRate(counter_currency string, region string) (float64, error) {
	exchangeRage := ExchangeRate{BaseCurrencyId: "USD", CounterCurrencyId: counter_currency, Region: region}
	if err := DB.Where("base_currency_id = ?", "USD").Where("counter_currency_id=?", counter_currency).Where("region = ?", region).First(&exchangeRage).Error; err != nil {
		return 0, err
	}

	return exchangeRage.Rate, nil
}

func GetRate(base_currenncy string, counter_currency string, region string) (float64, error) {
	exchangeRage := ExchangeRate{BaseCurrencyId: base_currenncy, CounterCurrencyId: counter_currency, Region: region}
	if err := DB.First(&exchangeRage).Error; err != nil {
		return 0, err
	}

	return exchangeRage.Rate, nil
}

var letterRunes = []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
var numRunes = []rune("1234567890")

func RandStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		if i%2 == 0 {
			b[i] = letterRunes[rand.Intn(len(letterRunes))]
		} else {
			b[i] = numRunes[rand.Intn(len(numRunes))]
		}
	}
	return string(b)
}

func genSn(n int) string {
	str := "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	b := len([]rune(str))
	x, y := n/b, n%b
	if x > 0 {
		return "A" + genSn(x) + str[y:y+1]
	} else {
		return "A" + str[y:y+1]
	}
}
