package service

import (
	"errors"
	"fmt"
	"st/models"
	"context"
	"strings"
)

type AppleChecker struct {
	IRefundChecker
}

func (a *AppleChecker) checkerWithCtx(ctx context.Context, depoistLog *models.GatewayDepositLog) (int, error) {
	result := make(chan int)
	err := make(chan error)

	go func() {
		state, error := a.checker(depoistLog)
		if error != nil {
			err <- error
		} else {
			result <- state
		}
	}()

	select {
	case <- ctx.Done():
		return 0, ctx.Err()
	case e := <- err:
		return 0, e
	case state:= <- result:
		return state, nil
	}
}

func (*AppleChecker) checker(depoistLog *models.GatewayDepositLog) (int, error) {
	var transaction_id string
	if len(depoistLog.External_id) > 9 {
		transaction_id = depoistLog.External_id[9:]
	} else {
		return 0, errors.New("Wrong format External_id.")
	}


	appleiap, err := models.FindAppleNotificationWithTransactionId(transaction_id)
	if err != nil {
		return 0, err
	}

	if appleiap.Status == 2 {
		// 已经处理过的订单
		return 0, nil
	} else if appleiap.Status == 1 {
		// 未处理的订单
		return 1, nil
	}

	return 0, err
}

func (*AppleChecker) proccess(depoistLog *models.GatewayDepositLog, status int) error {
	externalId := depoistLog.External_id
	transactionId := strings.Replace(externalId, "appleiap.", "", 1)
	return models.UpdateAppleNotificationStatus(transactionId, status)
}

func (*AppleChecker) proccessExternalId(external_id string, status int) error {
	return models.UpdateAppleNotificationStatus(external_id, status)
}

func (a *AppleChecker) checkOrderWithCtx(ctx context.Context, iap *models.GatewayAppleNotification) (*models.GatewayDepositLog, error) {
	externalId := fmt.Sprintf("appleiap.%s", iap.TransactionId)
	transaction, err := models.FindDepositLogWithExternalId(externalId)
	return transaction, err
}

func (a *AppleChecker) checkOrderWithCtxInYear(ctx context.Context, iap *models.GatewayAppleNotification) (*models.GatewayDepositLog, error) {
	externalId := fmt.Sprintf("appleiap.%s", iap.TransactionId)
	transaction, err := models.FindDepositLogWithExternalIdInYear(externalId)
	return transaction, err
}