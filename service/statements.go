package service

import (
	"fmt"
	jsoniter "github.com/json-iterator/go"
	log "github.com/sirupsen/logrus"
	"github.com/tealeg/xlsx"
	"io/ioutil"
	"net/http"
	"time"
)

type CheckoutStatementPayoutBreakdownProcessingFees struct {
	InterchangeFees float64 `json:"interchange_fees"`
	SchemeAndOtherNetworkFees float64 `json:"scheme_and_other_network_fees"`
	PremiumAndApmFees float64 `json:"premium_and_apm_fees"`
	ChargebackFees float64 `json:"chargeback_fees"`
	PayoutToCardFees float64 `json:"payout_to_card_fees"`
	PaymentGatewayFees float64 `json:"payment_gateway_fees"`
}

type CheckoutStatementPayoutBreakdown struct {
	ProcessedAmount float64 `json:"processed_amount"`
	RefundAmount float64 `json:"refund_amount"`
	ChargebackAmount float64 `json:"chargeback_amount"`
	PayoutsToCardAmount float64 `json:"payouts_to_card_amount"`
	ProcessingFees float64 `json:"processing_fees"`
	ProcessingFeesBreakdown CheckoutStatementPayoutBreakdownProcessingFees `json:"processing_fees_breakdown"`
}

type CheckoutStatementPayout struct {
	Currency string `json:"currency"`
	CarriedForwardAmount float64 `json:"carried_forward_amount"`
	CurrentPeriodAmount float64 `json:"current_period_amount"`
	NetAmount float64 `json:"net_amount"`
	Date string `json:"date"`
	PeriodStart string `json:"period_start"`
	PeriodEnd string `json:"period_end"`
	Id string `json:"id"`
	Status string `json:"status"`
	PayoutFee float64 `json:"payout_fee"`
	CurrentPeriodBreakdown CheckoutStatementPayoutBreakdown `json:"current_period_breakdown"`
	Links map[string]interface{} `json:"_links"`
}

type CheckoutStatementData struct {
	Id string `json:"id"`
	PeriodStart string `json:"period_start"`
	PeriodEnd string `json:"period_end"`
	Date string `json:"date"`
	Payouts []CheckoutStatementPayout `json:"payouts"`
}

type CheckoutStatementResponse struct {
	Count int `json:"count"`
	Data []CheckoutStatementData `json:"data"`
	Links map[string]interface{} `json:"_links"`
}

func CheckoutStatement(startdate string, enddate string)  {
	url := fmt.Sprintf("https://api.checkout.com/reporting/statements?include=payout_breakdown&from=%s&to=%s", startdate, enddate)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		panic(err)
	}
	//req.Header.Set("Authorization", config.Config.Finance.Checkout.SECRETKEY)
	req.Header.Set("Authorization", "sk_78d1963d-cae4-4e91-8da2-2d80b93fd76c")
	req.Header.Set("Content-Type", "application/json")

	client := http.Client{Timeout: 10*time.Second}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	defer resp.Body.Close()
	var respJson CheckoutStatementResponse
	var json_iterator = jsoniter.ConfigCompatibleWithStandardLibrary
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	json_iterator.Unmarshal(body, &respJson)
	count := respJson.Count
	log.WithFields(log.Fields{"resp": string(body), "count": count, "data": respJson.Data}).Info("http.get response")

	OutputCheckoutStatement(respJson)
}

func OutputCheckoutStatement(CheckoutStatement CheckoutStatementResponse) {
	file := xlsx.NewFile()
	sheet, err := file.AddSheet("statement")
	if err != nil {
		panic(err)
	}

	row := sheet.AddRow()
	nameCell := row.AddCell()
	nameCell.Value = "ID"

	payoutFeeCell := row.AddCell()
	payoutFeeCell.Value = "PayoutFee"

	statusCell := row.AddCell()
	statusCell.Value = "Status"

	netAmountCell := row.AddCell()
	netAmountCell.Value = "NetAmount"

	carriedCell := row.AddCell()
	carriedCell.Value = "CarriedForwardAmount"

	currentCell := row.AddCell()
	currentCell.Value = "CurrentPeriodAmount"

	currencyCell := row.AddCell()
	currencyCell.Value = "Currency"

	dateCell := row.AddCell()
	dateCell.Value = "Date"

	processAmountCell := row.AddCell()
	processAmountCell.Value = "ProcessedAmount"

	refundAmountCell := row.AddCell()
	refundAmountCell.Value = "RefundAmount"

	chargebackAmountCell := row.AddCell()
	chargebackAmountCell.Value = "ChargebackAmount"

	cardAmountCell := row.AddCell()
	cardAmountCell.Value = "PayoutsToCardAmount"

	processFeeCell := row.AddCell()
	processFeeCell.Value = "ProcessingFees"

	interchangeFeeCell := row.AddCell()
	interchangeFeeCell.Value = "InterchangeFees"

	schemeNetworkCell := row.AddCell()
	schemeNetworkCell.Value = "SchemeAndOtherNetworkFees"

	premiumApmCell := row.AddCell()
	premiumApmCell.Value = "PremiumAndApmFees"

	chargeBackFeeCell := row.AddCell()
	chargeBackFeeCell.Value = "ChargebackFees"

	payoutcardFeeCell := row.AddCell()
	payoutcardFeeCell.Value = "PayoutToCardFees"

	paymentGatewayFeeCell := row.AddCell()
	paymentGatewayFeeCell.Value = "PaymentGatewayFees"

	for _, data := range CheckoutStatement.Data {
		log.WithFields(log.Fields{"data": data}).Info("http.get response.data")

		for _, payout := range data.Payouts {
			log.WithFields(log.Fields{"payout.net_amount": payout.NetAmount}).Info("http.get response.data.payout")

			row := sheet.AddRow()
			nameCell := row.AddCell()
			nameCell.Value = payout.Id

			payoutFeeCell := row.AddCell()
			payoutFeeCell.Value = fmt.Sprintf("%f", payout.PayoutFee)

			statusCell := row.AddCell()
			statusCell.Value = payout.Status

			netAmountCell := row.AddCell()
			netAmountCell.Value = fmt.Sprintf("%f", payout.NetAmount)

			carriedCell := row.AddCell()
			carriedCell.Value = fmt.Sprintf("%f", payout.CarriedForwardAmount)

			currentCell := row.AddCell()
			currentCell.Value = fmt.Sprintf("%f", payout.CurrentPeriodAmount)

			currencyCell := row.AddCell()
			currencyCell.Value = payout.Currency

			dateCell := row.AddCell()
			dateCell.Value = payout.Date

			processAmountCell := row.AddCell()
			processAmountCell.Value = fmt.Sprintf("%f", payout.CurrentPeriodBreakdown.ProcessedAmount)

			refundAmountCell := row.AddCell()
			refundAmountCell.Value = fmt.Sprintf("%f", payout.CurrentPeriodBreakdown.RefundAmount)

			chargebackAmountCell := row.AddCell()
			chargebackAmountCell.Value = fmt.Sprintf("%f", payout.CurrentPeriodBreakdown.ChargebackAmount)

			cardAmountCell := row.AddCell()
			cardAmountCell.Value = fmt.Sprintf("%f", payout.CurrentPeriodBreakdown.PayoutsToCardAmount)

			processFeeCell := row.AddCell()
			processFeeCell.Value = fmt.Sprintf("%f", payout.CurrentPeriodBreakdown.ProcessingFees)

			interchangeFeeCell := row.AddCell()
			interchangeFeeCell.Value = fmt.Sprintf("%f", payout.CurrentPeriodBreakdown.ProcessingFeesBreakdown.InterchangeFees)

			schemeNetworkCell := row.AddCell()
			schemeNetworkCell.Value = fmt.Sprintf("%f", payout.CurrentPeriodBreakdown.ProcessingFeesBreakdown.SchemeAndOtherNetworkFees)

			premiumApmCell := row.AddCell()
			premiumApmCell.Value = fmt.Sprintf("%f", payout.CurrentPeriodBreakdown.ProcessingFeesBreakdown.PremiumAndApmFees)

			chargeBackFeeCell := row.AddCell()
			chargeBackFeeCell.Value = fmt.Sprintf("%f", payout.CurrentPeriodBreakdown.ProcessingFeesBreakdown.ChargebackFees)

			payoutcardFeeCell := row.AddCell()
			payoutcardFeeCell.Value = fmt.Sprintf("%f", payout.CurrentPeriodBreakdown.ProcessingFeesBreakdown.PayoutToCardFees)

			paymentGatewayFeeCell := row.AddCell()
			paymentGatewayFeeCell.Value = fmt.Sprintf("%f", payout.CurrentPeriodBreakdown.ProcessingFeesBreakdown.PaymentGatewayFees)


		}
	}

	err = file.Save("./checkout_statement.xlsx")
	if err != nil {
		panic(err)
	}
	log.Info("Write xls Success.")
}


func CheckoutPayment(startdate string, enddate string)  {
	url := fmt.Sprintf("https://api.checkout.com/reporting/payments?from=%s&to=%s", startdate, enddate)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		panic(err)
	}

	req.Header.Set("Authorization", "sk_78d1963d-cae4-4e91-8da2-2d80b93fd76c")
	req.Header.Set("Content-Type", "application/json")

	client := http.Client{Timeout: 10*time.Second}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	defer resp.Body.Close()
	var respJson CheckoutStatementResponse
	var json_iterator = jsoniter.ConfigCompatibleWithStandardLibrary
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	json_iterator.Unmarshal(body, &respJson)
	count := respJson.Count
	log.WithFields(log.Fields{"resp": string(body), "count": count, "data": respJson.Data}).Info("http.get response")

	CheckoutPaymentCSV(startdate, enddate)
	//OutputCheckoutStatement(respJson)
}

func CheckoutPaymentCSV(startdate string, enddate string) {
	url := fmt.Sprintf("https://api.checkout.com/reporting/payments/download?from=%s&to=%s", startdate, enddate)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		panic(err)
	}

	req.Header.Set("Authorization", "sk_78d1963d-cae4-4e91-8da2-2d80b93fd76c")
	req.Header.Set("Content-Type", "text/csv")

	client := http.Client{Timeout: 10*time.Second}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	defer resp.Body.Close()
	//var respJson CheckoutStatementResponse
	//var json_iterator = jsoniter.ConfigCompatibleWithStandardLibrary
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	//json_iterator.Unmarshal(body, &respJson)
	//count := respJson.Count
	log.WithFields(log.Fields{"resp": string(body)}).Info("http.get response")

	OutputCheckoutPaymentCSV(string(body))
}

func OutputCheckoutPaymentCSV(content string) error{
	err := ioutil.WriteFile("checkout_payment.csv", []byte(content), 0644)
	return err
}

func OutputCheckoutPayment(CheckoutStatement CheckoutStatementResponse) {
	file := xlsx.NewFile()
	sheet, err := file.AddSheet("statement")
	if err != nil {
		panic(err)
	}

	row := sheet.AddRow()
	nameCell := row.AddCell()
	nameCell.Value = "ID"

	payoutFeeCell := row.AddCell()
	payoutFeeCell.Value = "PayoutFee"

	statusCell := row.AddCell()
	statusCell.Value = "Status"

	netAmountCell := row.AddCell()
	netAmountCell.Value = "NetAmount"

	carriedCell := row.AddCell()
	carriedCell.Value = "CarriedForwardAmount"

	currentCell := row.AddCell()
	currentCell.Value = "CurrentPeriodAmount"

	currencyCell := row.AddCell()
	currencyCell.Value = "Currency"

	dateCell := row.AddCell()
	dateCell.Value = "Date"

	processAmountCell := row.AddCell()
	processAmountCell.Value = "ProcessedAmount"

	refundAmountCell := row.AddCell()
	refundAmountCell.Value = "RefundAmount"

	chargebackAmountCell := row.AddCell()
	chargebackAmountCell.Value = "ChargebackAmount"

	cardAmountCell := row.AddCell()
	cardAmountCell.Value = "PayoutsToCardAmount"

	processFeeCell := row.AddCell()
	processFeeCell.Value = "ProcessingFees"

	interchangeFeeCell := row.AddCell()
	interchangeFeeCell.Value = "InterchangeFees"

	schemeNetworkCell := row.AddCell()
	schemeNetworkCell.Value = "SchemeAndOtherNetworkFees"

	premiumApmCell := row.AddCell()
	premiumApmCell.Value = "PremiumAndApmFees"

	chargeBackFeeCell := row.AddCell()
	chargeBackFeeCell.Value = "ChargebackFees"

	payoutcardFeeCell := row.AddCell()
	payoutcardFeeCell.Value = "PayoutToCardFees"

	paymentGatewayFeeCell := row.AddCell()
	paymentGatewayFeeCell.Value = "PaymentGatewayFees"

	for _, data := range CheckoutStatement.Data {
		log.WithFields(log.Fields{"data": data}).Info("http.get response.data")

		for _, payout := range data.Payouts {
			log.WithFields(log.Fields{"payout.net_amount": payout.NetAmount}).Info("http.get response.data.payout")

			row := sheet.AddRow()
			nameCell := row.AddCell()
			nameCell.Value = payout.Id

			payoutFeeCell := row.AddCell()
			payoutFeeCell.Value = fmt.Sprintf("%f", payout.PayoutFee)

			statusCell := row.AddCell()
			statusCell.Value = payout.Status

			netAmountCell := row.AddCell()
			netAmountCell.Value = fmt.Sprintf("%f", payout.NetAmount)

			carriedCell := row.AddCell()
			carriedCell.Value = fmt.Sprintf("%f", payout.CarriedForwardAmount)

			currentCell := row.AddCell()
			currentCell.Value = fmt.Sprintf("%f", payout.CurrentPeriodAmount)

			currencyCell := row.AddCell()
			currencyCell.Value = payout.Currency

			dateCell := row.AddCell()
			dateCell.Value = payout.Date

			processAmountCell := row.AddCell()
			processAmountCell.Value = fmt.Sprintf("%f", payout.CurrentPeriodBreakdown.ProcessedAmount)

			refundAmountCell := row.AddCell()
			refundAmountCell.Value = fmt.Sprintf("%f", payout.CurrentPeriodBreakdown.RefundAmount)

			chargebackAmountCell := row.AddCell()
			chargebackAmountCell.Value = fmt.Sprintf("%f", payout.CurrentPeriodBreakdown.ChargebackAmount)

			cardAmountCell := row.AddCell()
			cardAmountCell.Value = fmt.Sprintf("%f", payout.CurrentPeriodBreakdown.PayoutsToCardAmount)

			processFeeCell := row.AddCell()
			processFeeCell.Value = fmt.Sprintf("%f", payout.CurrentPeriodBreakdown.ProcessingFees)

			interchangeFeeCell := row.AddCell()
			interchangeFeeCell.Value = fmt.Sprintf("%f", payout.CurrentPeriodBreakdown.ProcessingFeesBreakdown.InterchangeFees)

			schemeNetworkCell := row.AddCell()
			schemeNetworkCell.Value = fmt.Sprintf("%f", payout.CurrentPeriodBreakdown.ProcessingFeesBreakdown.SchemeAndOtherNetworkFees)

			premiumApmCell := row.AddCell()
			premiumApmCell.Value = fmt.Sprintf("%f", payout.CurrentPeriodBreakdown.ProcessingFeesBreakdown.PremiumAndApmFees)

			chargeBackFeeCell := row.AddCell()
			chargeBackFeeCell.Value = fmt.Sprintf("%f", payout.CurrentPeriodBreakdown.ProcessingFeesBreakdown.ChargebackFees)

			payoutcardFeeCell := row.AddCell()
			payoutcardFeeCell.Value = fmt.Sprintf("%f", payout.CurrentPeriodBreakdown.ProcessingFeesBreakdown.PayoutToCardFees)

			paymentGatewayFeeCell := row.AddCell()
			paymentGatewayFeeCell.Value = fmt.Sprintf("%f", payout.CurrentPeriodBreakdown.ProcessingFeesBreakdown.PaymentGatewayFees)


		}
	}

	err = file.Save("./checkout_statement.xlsx")
	if err != nil {
		panic(err)
	}
	log.Info("Write xls Success.")
}



