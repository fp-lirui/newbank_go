package service

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"st/config"
	"st/utils"
	"strings"
	"time"

	jsoniter "github.com/json-iterator/go"
	log "github.com/sirupsen/logrus"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/androidpublisher/v3"
	models "st/models"
)

type IRefundChecker interface {
	checker(depoistLog *models.GatewayDepositLog) (int, error)
	checkerWithCtx(ctx context.Context, depoistLog *models.GatewayDepositLog) (int, error)
	proccess(depoistLog *models.GatewayDepositLog, status int) error
	proccessExternalId(external_id string, status int) error
}

type GoogleChecker struct {
	IRefundChecker
}

func (a *GoogleChecker) checkerWithCtx(ctx context.Context, depoistLog *models.GatewayDepositLog) (int, error) {
	result := make(chan int)
	err := make(chan error)

	go func() {
		state, error := a.checker(depoistLog)
		if error != nil {
			err <- error
		} else {
			result <- state
		}
	}()

	select {
		case <- ctx.Done():
			return 0, ctx.Err()
		case e := <- err:
			return 0, e
		case state:= <- result:
			return state, nil
	}
}

func (*GoogleChecker) checker(depoistLog *models.GatewayDepositLog) (int, error) {
	//external_id := depoistLog.External_id
	package_id := depoistLog.Package_id
	memo := depoistLog.Memo
	pkgName := "chat.meme.inke"
	pakeIndex := UnicodeIndex(package_id, "default.")
	if pakeIndex > 0 {
		pkgName = SubString(package_id, 0, UnicodeIndex(package_id, "default.")-1)
	}
	//gateway := depoistLog.Gateway
	//order_id := external_id[UnicodeIndex(external_id, "googleplayiab.")+14:]

	memoStr := memo[UnicodeIndex(memo, "Body:")+5:]
	memoStr = strings.ReplaceAll(memoStr, `\`, "")
	memoStr = strings.ReplaceAll(memoStr, "\n", "")
	memoStr = strings.ReplaceAll(memoStr, "\"{", "{")
	memoStr = strings.ReplaceAll(memoStr, "}\"", "}")
	//val := []byte(`{"type": "googleplayiab","user_id":"1612030","account":"default","receipt":{"passthrough":"","payload":{"orderId":"GPA.3312-5942-5403-20828","packageName":"chat.meme.inke","productId":"chat.meme.inke.default.bbcoin.tier6","purchaseTime":1594116290645,"purchaseState":0,"purchaseToken":"fbbdjhghfejidphnbljmebjc.AO-J1OwdA7kmXKYUZ2sIrue1A1OdDkC_TC2YaRPLs54yiuQk2vNQjmSOZvPPw2Lc9kMtt-nwtg6dUfKsngRp5xXVV_vlfnvboxe1dIk9wN51S-IA4PRe0lb4Y290BPuKt3LIm4MnIBDMqiNsJVpb3p9v1BoDeUvg2g","acknowledged":false},"sign":"fykpLVsUjO/cfZTuqiuSZ7gkJnvGD1uS2G7WDRP/9gKn3dcPpHl0x8yL66YugzABmdl419fBpUYUlTf86Q1mt7FxdWeYUK+luiAYb6nCT/cGIWLWkQpMFFQ1G6nm3AavTyvpCS+lieZSEiuOC8FTMex7bfvJs3B78ooW5VZIQnmJNHSbbLELg5rH6om0ED+h3+RuMPbxSOTPDmSEoaPGswB3KN6IG+NAU4QKvUJmemtY68cHxcBvG4rxGZ6k1EAjvLbd9uCfOHzEVXpjDMUXnqukPycUCESU7B8by+LiKU5gg8gGEEGgcUTm83y5KqrfmY8bVrjPlQ8nl+1DC9/5cg==","region":"CN","tag":"default"},"preorder_id":"pre-2b99e6f1f59fdc3f59387f57fc16f8","currency":"HKD","amount":"788"}`)
	val := []byte(memoStr)
	var memoJson map[string]interface{}
	var json_iterator = jsoniter.ConfigCompatibleWithStandardLibrary
	json_iterator.Unmarshal(val, &memoJson)
	receipt := memoJson["receipt"]
	payload := receipt.(map[string]interface{})["payload"]
	purchaseToken := payload.(map[string]interface{})["purchaseToken"]

	return PurchasesProductCheckerWithCredentials(package_id, purchaseToken.(string), pkgName)
}

func (*GoogleChecker) proccess(depoistLog *models.GatewayDepositLog, status int) error {
	//googleplayiab.GPA.3353-0301-2146-84395
	err3 := models.UpdateGoogleVoidedPurchasesStatus(depoistLog.External_id[14:], status)
	return err3
}

func (*GoogleChecker) proccessExternalId(external_id string, status int) error {
	//googleplayiab.GPA.3353-0301-2146-84395
	err3 := models.UpdateGoogleVoidedPurchasesStatus(external_id, status)
	return err3
}

func requestToken() (string, error) {
	urlValues := url.Values{}
	urlValues.Add("client_id", config.Config.GoogleAPP.Default.ClientID)
	urlValues.Add("client_secret", config.Config.GoogleAPP.Default.SecurtKey)
	urlValues.Add("refresh_token", config.Config.GoogleAPP.Default.RefreshToken)
	urlValues.Add("grant_type", "refresh_token")
	resp, err := http.PostForm("https://accounts.google.com/o/oauth2/token", urlValues)
	if err != nil {
		log.WithFields(log.Fields{"resp": resp, "err": err}).Error("http.post error")
		return "", err
	}

	body, err1 := ioutil.ReadAll(resp.Body)
	if err1 != nil {
		log.WithFields(log.Fields{"body": body, "err": err1}).Error("http.post readbody error")
		return "", err1
	}
	var result map[string]string
	json.Unmarshal(body, &result)
	log.WithFields(log.Fields{
		"result": result,
	}).Warn("http.post result")
	access_token := result["access_token"]
	expires_in := result["expires_in"]
	log.WithFields(log.Fields{
		"access_token": access_token,
		"expires_in":   expires_in,
	}).Warn("http.post result")

	utils.Exec("set", "access_token", access_token)
	utils.Exec("EXPIRE", "access_token", expires_in)

	return access_token, nil
}

func OrderChecker() error {
	access_token, err := utils.StringGet("access_token")
	log.WithFields(log.Fields{
		"access_token": access_token,
		"err":          err,
	}).Warn("read access_token from redis")
	if access_token == "" {
		access_token, _ = requestToken()
	}



	packagename := []string{"com.nett.social", "chat.meme.perky"}
	url := "https://www.googleapis.com/androidpublisher/v3/applications/%s/purchases/voidedpurchases?access_token=%s&startTime=%s"
	startTime := time.Now().Unix() - 3*24*60*60
	url = fmt.Sprintf(url, packagename[0], access_token, startTime)

	resp, err := http.Get(url)
	if err != nil {
		log.WithFields(log.Fields{"resp": resp, "err": err}).Error("http.get error")
		return err
	}
	body, err1 := ioutil.ReadAll(resp.Body)
	if err1 != nil {
		log.WithFields(log.Fields{"body": body, "err": err1}).Error("http.get readbody error")
		return err1
	}
	var result map[string]interface{}
	json.Unmarshal(body, &result)
	log.WithFields(log.Fields{
		"result": result,
	}).Warn("http.get result")

	voidedPurchases := result["voidedPurchases"].([]interface{})
	for _, value := range voidedPurchases {
		voidedPurchase, _ := value.(map[string]interface{})
		kind := voidedPurchase["kind"]
		purchaseToken := voidedPurchase["purchaseToken"]
		purchaseTimeMillis := voidedPurchase["purchaseTimeMillis"]
		voidedTimeMillis := voidedPurchase["voidedTimeMillis"]
		log.WithFields(log.Fields{"kind": kind, "purchaseToken": purchaseToken, "purchaseTimeMillis": purchaseTimeMillis, "voidedTimeMillis": voidedTimeMillis}).Error("parse voidedPurchase error")
	}

	return nil
}

func OrderCheckerWithCredentials(initial bool, packagename string, credenials string) (*models.GatewayGoogleplayVoidedpurchase, error) {
	body, err2 := ioutil.ReadFile(credenials)
	if err2 != nil {
		log.WithFields(log.Fields{"err": err2}).Error("ioutil ReadFile error")
		return nil, err2
	}

	conf, err3 := google.JWTConfigFromJSON(body, androidpublisher.AndroidpublisherScope)
	if err3 != nil {
		return nil, err3
	}
	client := conf.Client(oauth2.NoContext)

	service, err3 := androidpublisher.New(client)

	if err3 != nil {
		log.WithFields(log.Fields{"err": err3}).Error("androidpublisher.NewService error")
		return nil, err3
	}
	log.WithFields(log.Fields{
		"service": service,
	}).Warn("androidpublisher.New")

	s := androidpublisher.NewPurchasesVoidedpurchasesService(service)
	//purchaseService := androidpublisherService.Purchases
	call := s.List(packagename)
	if !initial {
		call.StartTime((time.Now().UnixNano()/1e6 - 48*60*60*1000))
	}
	log.WithFields(log.Fields{
		"voidedpurchaseListCall": call,
	}).Warn("purchaseService Voidedpurchases listCall")
	response, _ := call.Do()
	log.WithFields(log.Fields{
		"voidedpurchaseListCall response": response,
	}).Warn("purchaseService Voidedpurchases listCall.DO")

	voidedPurchases := response.VoidedPurchases
	if initial {
		SaveVoidedPurchaseDB(voidedPurchases)
	} else {
		for _, value := range voidedPurchases {

			voidedPurchase := value
			kind := voidedPurchase.Kind
			orderId := voidedPurchase.OrderId
			purchaseToken := voidedPurchase.PurchaseToken
			purchaseTimeMillis := voidedPurchase.PurchaseTimeMillis
			voidedReason := voidedPurchase.VoidedReason
			voidedSource := voidedPurchase.VoidedSource
			voidedTimeMillis := voidedPurchase.VoidedTimeMillis

			gateway_voidedpurchase, err := models.SaveVoidedPurchase(orderId, purchaseToken, purchaseTimeMillis, voidedReason, voidedSource, voidedTimeMillis)
			if err == nil {
				log.WithFields(log.Fields{
					"kind":          kind,
					"orderId":       orderId,
					"purchaseToken": purchaseToken,
					"purchaseTimeMillis": purchaseTimeMillis,
					"voidedReason": voidedReason,
					"voidedSource": voidedSource,
					"voidedTimeMillis": voidedTimeMillis,
				}).Warn("VoidedPurchase info")
				return gateway_voidedpurchase, nil
			}

		}
	}



	return nil, nil
}

func PurchasesProductCheckerWithCredentials(productID string, purchasesToken string, pkgName string) (int, error) {
	ex, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	var pemFile string
	if strings.Contains(pkgName, "perky") {
		pemFile = config.Config.GoogleAPP.JP.Keys
	} else if strings.Contains(pkgName, "default") {
		pemFile = config.Config.GoogleAPP.Default.Keys
	} else if strings.Contains(pkgName, "inke") {
		pemFile = config.Config.GoogleAPP.Inke.Keys
	} else {
		panic("Not found google package name")
	}
	pemfilepath := fmt.Sprintf("%s/config/%s", ex, pemFile )
	body, err2 := ioutil.ReadFile(pemfilepath)
	if err2 != nil {
		log.WithFields(log.Fields{"err": err2}).Error("ioutil ReadFile error")
		return 0, err2
	}

	conf, err3 := google.JWTConfigFromJSON(body, androidpublisher.AndroidpublisherScope)
	if err3 != nil {
		return 0, err3
	}
	client := conf.Client(oauth2.NoContext)

	service, err3 := androidpublisher.New(client)

	if err3 != nil {
		log.WithFields(log.Fields{"err": err3}).Error("androidpublisher.NewService error")
		return 0, err3
	}

	//pkgName = "chat.meme.inke"
	//productID = "chat.meme.inke.default.bbcoin.tier6"
	//purchasesToken = "fdegegehdogocjakcgfmaigb.AO-J1Ox4Ummo4872fb8S_gdb6ZoYls35yn6VKLU8RG5hzQacIScqheWkSHKnf_1tR_z9BSfiq7v64Ze_cSAaf3rSxouCT35KGmUIxlkV6_qjTCBs_TVwIedPPTXT5wE2sIQjlaJVSMsuUgW0Tz_NTavfc32uE39Ryg\n\n"
	log.WithFields(log.Fields{
		"pkgName":pkgName,
		"productID":productID,
		"purchasesToken":purchasesToken,
	}).Warn("purchaseService Voidedpurchases listCall.DO")
	s := androidpublisher.NewPurchasesProductsService(service)
	call := s.Get(pkgName, productID, purchasesToken)

	response, err := call.Do()
	if err!= nil {
		return 0, err
	}
	log.WithFields(log.Fields{
		"voidedpurchaseListCall response": response,
	}).Warn("purchaseService Voidedpurchases listCall.DO")

	//consumption := response.ConsumptionState

	purchaseState := response.PurchaseState

	return int(purchaseState), nil

}

func SaveVoidedPurchaseDB(voidedPurchases []*androidpublisher.VoidedPurchase) error {
	defer func() {
		if err := recover(); err != nil {
			panic("SaveVoidedPurchaseDB() err")
		}
	}()

	//构建SQL语句
	sql_bat := `insert into gateway_googleplay_voidedpurchase(order_id, purchase_token, purchase_time_millis, voided_reason, voided_source, voided_time_millis, created_at) values`

	for _, vp := range voidedPurchases {
		value := `("` + vp.OrderId + `","` + vp.PurchaseToken + `",` + fmt.Sprintf("%d", vp.PurchaseTimeMillis) + `,` + fmt.Sprintf("%d", vp.VoidedReason) + `,` + fmt.Sprintf("%d", vp.VoidedSource) + `,`+ fmt.Sprintf("%d", vp.VoidedTimeMillis) +`,` + fmt.Sprintf("%d", time.Now().Unix()) +`),`

		sql_bat += value
	}

	sql_bat = sql_bat[:len(sql_bat)-1] + ";"

	log.WithFields(log.Fields{
		"sql":          sql_bat,
	}).Warn("SaveVoidedPurchaseDB")

	models.DB.Exec(sql_bat)

	return nil
}


func SubString(str string, begin, length int) (substr string) {
	// 将字符串的转换成[]rune
	rs := []rune(str)
	lth := len(rs)

	// 简单的越界判断
	if begin < 0 {
		begin = 0
	}
	if begin >= lth {
		begin = lth
	}
	end := begin + length
	if end > lth {
		end = lth
	}

	// 返回子串
	return string(rs[begin:end])
}

func UnicodeIndex(str, substr string) int {
	// 子串在字符串的字节位置
	result := strings.Index(str, substr)
	if result >= 0 {
		// 获得子串之前的字符串并转换成[]byte
		prefix := []byte(str)[0:result]
		// 将子串之前的字符串转换成[]rune
		rs := []rune(string(prefix))
		// 获得子串之前的字符串的长度，便是子串在字符串的字符位置
		result = len(rs)
	}

	return result
}
