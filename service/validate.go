package service

import (
	"fmt"
	models "st/models"
	"strconv"
	"strings"
)

func ValidateCoreaccount() {
	ReadAccounttomap()
	fmt.Println("newcore:", CoreAccountMap)
	cursor := 0
	resultsize := 100000

	var count int

	models.DB.Table("core_account").Count(&count)

	fmt.Println("count11:", count)
	var oldcoreaccountMap = make(map[string]int64)
	sql := `select user_id, name, currency_id, balance from core_account order by user_id limit ?, ?`
	loopcount := count/resultsize + 1
	for i := 0; i < loopcount; i++ {

		rows, err := models.DB.Raw(sql, cursor, resultsize).Rows()
		defer rows.Close()
		if err != nil {
			fmt.Println("read diamond error: ", err)
			return
		}

		defer func() {
			if err := recover(); err != nil {
				fmt.Println("validate_coreaccount() error: ", err, "!!!!!!!!!!")
				panic(err)
			}
		}()

		for rows.Next() {
			var (
				userID      string
				account     string
				currency    string
				corebalance float64
				oldbalance  int64
			)
			err1 := rows.Scan(&userID, &account, &currency, &corebalance)
			if err1 != nil {
				panic(string("rows.scan() err1:" + err1.Error()))
			}

			oldbalance = int64(corebalance)
			accountkey := userID + ":" + strings.ToLower(account) + ":" + currency

			if _, ok := CoreAccountMap[accountkey]; ok {
				accountBalance := CoreAccountMap[accountkey]
				fmt.Println("1111", accountkey, accountBalance)
				if accountBalance > oldbalance {
					// 算多了， 减去
					fmt.Println("NewAccount UserID:" + userID + "(" + account + ")=" + strconv.FormatInt(accountBalance, 10) + "  more than Core_Account" + strconv.FormatInt(oldbalance, 10) + ".")

					CoreAccountMap[accountkey] = oldbalance

				} else if accountBalance < oldbalance {
					// 算少了， 加上
					fmt.Println("NewAccount UserID:" + userID + "(" + account + ")=" + strconv.FormatInt(accountBalance, 10) + "  less than Core_Account" + strconv.FormatInt(oldbalance, 10) + ".")

					CoreAccountMap[accountkey] = oldbalance
				} else {
					// 相等
				}
			} else {
				// 没有找到
				fmt.Println("NewAccount UserID:" + userID + "(" + account + ")=" + strconv.FormatInt(oldbalance, 10) + "  not found.")
				CoreAccountMap[accountkey] = oldbalance
			}

			oldcoreaccountMap[accountkey] = oldbalance

		}

		cursor += resultsize
	}
	fmt.Println("oldcore:", oldcoreaccountMap)
	for k, v := range CoreAccountMap {
		if _, ok := oldcoreaccountMap[k]; ok {
			// 存在就行
		} else {
			// 不存在的删除
			fmt.Println("CoreAccount " + k + "=" + strconv.FormatInt(v, 10) + "  not found.")
			delete(CoreAccountMap, k)
		}
	}

	SaveCoreAccountToDB()

}

func ValidateWallet() {
	Readdiamondtomap()
	Readtickettomap()
	ReadAccounttomap()

	for k, v := range CoreAccountMap {
		userID := strings.Split(k, ":")[0]
		account := strings.Split(k, ":")[1]
		currency := strings.Split(k, ":")[2]

		coreBalance := int64(v)

		walletkey := userID + ":" + account

		if currency == "DIAMOND" {

			if _, ok := WalletDiamondMap[walletkey]; ok {
				diamondList := WalletDiamondMap[walletkey]
				// 计算wallet diamond 总数
				var diamondBalance int64
				for _, v1 := range diamondList {
					snum := strings.Split(v1.(string), ":")[1]
					dnumint, err := strconv.ParseInt(snum, 10, 64)
					if err != nil {
						panic(string("wrong format diamondmap : " + userID + " " + account))
					}
					diamondBalance = diamondBalance + dnumint
				}
				// 对比 core 和 wallet
				if diamondBalance > coreBalance {
					fmt.Println("DIAMOND UserID:" + userID + "(" + account + ")  more than CoreAccount")
					// diamond 多了
					delKey := -1
					minusBalance := diamondBalance - coreBalance
					for k2, v2 := range diamondList {
						ssn := strings.Split(v2.(string), ":")[0]
						snum := strings.Split(v2.(string), ":")[1]
						dnumint, err := strconv.ParseInt(snum, 10, 64)
						if err != nil {
							panic(string("wrong format diamondmap : " + userID + " " + account))
						}

						if dnumint > minusBalance {
							diamondList[k2] = ssn + ":" + strconv.FormatInt(dnumint-minusBalance, 10)
							minusBalance = 0
							break
						} else if dnumint <= minusBalance {
							minusBalance = minusBalance - dnumint
							delKey = k2
							if minusBalance == 0 {
								break
							} else {
								continue
							}
						}
					}
					if delKey >= 0 {
						if len(diamondList) > delKey+1 {
							diamondList = diamondList[delKey+1:]
							WalletDiamondMap[walletkey] = diamondList
						} else {
							diamondList = nil
							WalletDiamondMap[walletkey] = nil
						}
					}
					if minusBalance > 0 {
						panic(string("Error: " + userID + " " + account + " not enough"))
					}
				} else if diamondBalance < coreBalance {
					fmt.Println("DIAMOND UserID:" + userID + "(" + account + ")  less than CoreAccount")
					// diamond 少了
					dicmondVal := diamondList[0]
					if strings.HasPrefix(dicmondVal.(string), "N0:") {
						snum := strings.Split(dicmondVal.(string), ":")[1]
						tnumint, err := strconv.ParseInt(snum, 10, 64)
						if err != nil {
							panic(string("Error: wrong format diamondmap : " + userID + " (" + account + ")"))
						}
						dicmondVal = "N0" + ":" + strconv.FormatInt(coreBalance-diamondBalance+tnumint, 10)
						diamondList[0] = dicmondVal
					} else {
						dicmondVal = "N0:" + strconv.FormatInt(coreBalance-diamondBalance, 10)
						diamondList = append(diamondList, dicmondVal)
						copy(diamondList[1:], diamondList[0:])
						diamondList[0] = dicmondVal
					}

					WalletDiamondMap[walletkey] = diamondList
				} else {
					// 刚好
					fmt.Println("DIAMOND UserID:" + userID + "(" + account + ")  equals CoreAccount")
				}
			} else {
				fmt.Println("DIAMOND UserID:" + userID + "(" + account + ") Not found. Create new one.")
				// wallet 中没有这个账户，直接加一个
				diamondValue := "N0:" + strconv.FormatInt(coreBalance, 10)
				diamondValueList := make([]interface{}, 1)
				diamondValueList[0] = diamondValue

				WalletDiamondMap[walletkey] = diamondValueList
			}
		}
		if currency == "TICKET" {

			if _, ok := WalletTicketMap[walletkey]; ok {
				ticketList := WalletTicketMap[walletkey]
				// 计算wallet ticket 总数
				var ticketBalance int64
				for _, v1 := range ticketList {
					snum := strings.Split(v1.(string), ":")[1]
					dnumint, err := strconv.ParseInt(snum, 10, 64)
					if err != nil {
						panic(string("wrong format ticketmap : " + userID + " " + account))
					}
					ticketBalance = ticketBalance + dnumint
				}
				// 对比 core 和 wallet
				if ticketBalance > coreBalance {
					fmt.Println("TICKT UserID:" + userID + "(" + account + ")  more than CoreAccount")
					// ticket 多了
					delKey := -1
					minusBalance := ticketBalance - coreBalance
					for k2, v2 := range ticketList {
						ssn := strings.Split(v2.(string), ":")[0]
						snum := strings.Split(v2.(string), ":")[1]
						dnumint, err := strconv.ParseInt(snum, 10, 64)
						if err != nil {
							panic(string("wrong format ticketmap : " + userID + " " + account))
						}

						if dnumint > minusBalance {
							ticketList[k2] = ssn + ":" + strconv.FormatInt(dnumint-minusBalance, 10)
							minusBalance = 0
							break
						} else if dnumint <= minusBalance {
							minusBalance = minusBalance - dnumint
							delKey = k2
							if minusBalance == 0 {
								break
							} else {
								continue
							}
						}
					}
					if delKey >= 0 {
						if len(ticketList) > delKey+1 {
							ticketList = ticketList[delKey+1:]
							WalletTicketMap[walletkey] = ticketList
						} else {
							ticketList = nil
							WalletTicketMap[walletkey] = nil
						}
					}
					if minusBalance > 0 {
						panic(string("Error: " + userID + " " + account + " not enough"))
					}
				} else if ticketBalance < coreBalance {
					fmt.Println("TICKET UserID:" + userID + "(" + account + ")  less than CoreAccount")
					// tickete 少了
					ticketVal := ticketList[0]
					if strings.HasPrefix(ticketVal.(string), "N0:") {
						snum := strings.Split(ticketVal.(string), ":")[1]
						tnumint, err := strconv.ParseInt(snum, 10, 64)
						if err != nil {
							panic(string("Error: wrong format ticketmap : " + userID + " (" + account + ")"))
						}
						ticketVal = "N0" + ":" + strconv.FormatInt(coreBalance-ticketBalance+tnumint, 10)
						ticketList[0] = ticketVal
					} else {
						ticketVal = "N0:" + strconv.FormatInt(coreBalance-ticketBalance, 10)
						ticketList = append(ticketList, ticketVal)
						copy(ticketList[1:], ticketList[0:])
						ticketList[0] = ticketVal
					}

					WalletTicketMap[walletkey] = ticketList
				} else {
					// 刚好
					fmt.Println("TICKET UserID:" + userID + "(" + account + ")  equals CoreAccount")
				}
			} else {
				fmt.Println("TICKET UserID:" + userID + "(" + account + ") Not found. Create new one.")
				// wallet 中没有这个账户，直接加一个
				ticketValue := "N0:" + strconv.FormatInt(coreBalance, 10)
				ticketValueList := make([]interface{}, 1)
				ticketValueList[0] = ticketValue

				WalletTicketMap[walletkey] = ticketValueList
			}
		}

	}

	SaveWalletDiamondToDB()
	SaveWalletTicketToDB()

}
