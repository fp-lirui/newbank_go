
1. 创建 walletdiamond表
CREATE TABLE `newbank_walletdiamond` (
  `id` int(11) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `sn` varchar(255) NOT NULL,
  `account` varchar(255) NOT NULL,
  `num` bigint(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `sn` (`sn`),
  KEY `account` (`account`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
 PARTITION BY LINEAR HASH (user_id)
(PARTITION p0 ENGINE = InnoDB,
 PARTITION p1 ENGINE = InnoDB,
 PARTITION p2 ENGINE = InnoDB,
 PARTITION p3 ENGINE = InnoDB,
 PARTITION p4 ENGINE = InnoDB,
 PARTITION p5 ENGINE = InnoDB,
 PARTITION p6 ENGINE = InnoDB,
 PARTITION p7 ENGINE = InnoDB,
 PARTITION p8 ENGINE = InnoDB,
 PARTITION p9 ENGINE = InnoDB)

2. 创建 walletticket 表
CREATE TABLE `newbank_walletticket` (
  `id` int(11) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `sn` varchar(255) NOT NULL,
  `account` varchar(255) NOT NULL,
  `num` bigint(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `sn` (`sn`),
  KEY `account` (`account`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
PARTITION BY HASH (user_id)
(PARTITION p0 ENGINE = InnoDB,
 PARTITION p1 ENGINE = InnoDB,
 PARTITION p2 ENGINE = InnoDB,
 PARTITION p3 ENGINE = InnoDB,
 PARTITION p4 ENGINE = InnoDB,
 PARTITION p5 ENGINE = InnoDB,
 PARTITION p6 ENGINE = InnoDB,
 PARTITION p7 ENGINE = InnoDB,
 PARTITION p8 ENGINE = InnoDB,
 PARTITION p9 ENGINE = InnoDB,
 PARTITION p10 ENGINE = InnoDB,
 PARTITION p11 ENGINE = InnoDB,
 PARTITION p12 ENGINE = InnoDB,
 PARTITION p13 ENGINE = InnoDB,
 PARTITION p14 ENGINE = InnoDB,
 PARTITION p15 ENGINE = InnoDB,
 PARTITION p16 ENGINE = InnoDB,
 PARTITION p17 ENGINE = InnoDB,
 PARTITION p18 ENGINE = InnoDB,
 PARTITION p19 ENGINE = InnoDB)

 3. 创建 core_account 表
 CREATE TABLE `newbank_coreaccount` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `currency` varchar(10) NOT NULL,
  `balance` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `name` (`name`),
  KEY `currency` (`currency`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8


4. 创建 snconfig 表
CREATE TABLE `newbank_snconfig` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sn` varchar(50) NOT NULL,
  `channel` varchar(250) NOT NULL,
  `package_id` varchar(250) NOT NULL,
  `amount` decimal(25,10) NOT NULL,
  `bundle` bigint(20) NOT NULL,
  `value` decimal(25,10) NOT NULL,
  `currency` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sn` (`sn`),
  UNIQUE KEY `channel` (`channel`,`package_id`),
  KEY `package_id` (`package_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7024 DEFAULT CHARSET=utf8

5. 创建配置表
CREATE TABLE `newbank_walletdataconfig` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `param` varchar(20) NOT NULL,
  `value` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `param` (`param`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

------- 以下实现自维护的自增id  -------

1. 新建序列表
drop table if exists newbank_sequence;  
create table newbank_sequence (      
seq_name        VARCHAR(50) NOT NULL, -- 序列名称      
current_val     INT         NOT NULL, -- 当前值      
increment_val   INT         NOT NULL    DEFAULT 1, -- 步长(跨度)      
PRIMARY KEY (seq_name)   ); 


2. 新增一个序列
INSERT INTO newbank_sequence VALUES ('walletdiamond', '0', '1');
INSERT INTO newbank_sequence VALUES ('walletticket', '0', '1');

3. 创建 函数 用于获取序列当前值(v_seq_name 参数值 代表序列名称)
DELIMITER $$
create function currval(v_seq_name VARCHAR(50))
returns integer 
begin     
    declare value integer;      
    set value = 0;      
    select current_val into value  from newbank_sequence where seq_name = v_seq_name;
    return value;
end $$
DELIMITER ;


4. 创建nextval函数 用于获取序列下一个值(v_seq_name 参数值 代表序列名称)
DELIMITER $$
create function nextval (v_seq_name VARCHAR(50))
    returns integer
begin
    update newbank_sequence set current_val = current_val + increment_val  where seq_name = v_seq_name;
    return currval(v_seq_name);
end$$
DELIMITER ;

5. 新建触发器 tri_walletdiamond 插入新纪录前给自增字段赋值实现字段自增效果
DELIMITER $$
CREATE TRIGGER `TRI_walletdiamond` BEFORE INSERT ON `newbank_walletdiamond` FOR EACH ROW BEGIN
set NEW.id = nextval('walletdiamond');
END $$
DELIMITER ;

6. 新建触发器tri_walletticket 插入新纪录前给自增字段赋值实现字段自增效果
DELIMITER $$
CREATE TRIGGER `TRI_walletticket` BEFORE INSERT ON `newbank_walletticket` FOR EACH ROW BEGIN
set NEW.id = nextval('walletticket');
END $$
DELIMITER ;




// 验证币 wallet and account
select a.*, b.*, a.c+b.c, d.*, (a.c+b.c-d.c) from (SELECT sum(num) c FROM `newbank_walletdiamond_1`) a, (SELECT sum(num) c FROM `newbank_walletdiamond_0`)b, (select sum(balance) c from newbank_coreaccount where currency='DIAMOND' and user_id!='undefined') d;


// 验证豆 wallet and account
select a.*, b.*, a.c+b.c, d.*, (a.c+b.c-d.c) from (SELECT sum(num) c FROM `newbank_walletticket_1`) a, (SELECT sum(num) c FROM `newbank_walletticket_0`)b, (select sum(balance) c from newbank_coreaccount where currency='TICKET' and user_id!='undefined') d;


// 按用户验证币
SELECT d1.*, d2.*, d1.num-d2.num from (SELECT sum(num) num, user_id, account FROM `newbank_walletdiamond_0` a GROUP BY user_id, account) as d1, (SELECT sum(balance) num, user_id, name from `newbank_coreaccount` b where b.currency = 'DIAMOND' GROUP BY user_id, name) as d2 where d1.user_id = d2.user_id and d1.num != d2.num and d1.account = d2.name;

// 按用户验证豆
SELECT d1.*, d2.*, d1.num-d2.num from (SELECT sum(num) num, user_id FROM `newbank_walletticket_0` a GROUP BY user_id) as d1, (SELECT sum(balance) num, user_id from `newbank_coreaccount` b where b.currency = 'TICKET' GROUP BY user_id) as d2 where d1.user_id = d2.user_id and d1.num != d2.num


// 按照 user_id 对比 newcore 和 oldcore DIAMOND
select old.user_id,old.name,old.balance,ne.balance, old.balance-ne.balance from core_account old, newbank_coreaccount ne where 1=1 and old.user_id = ne.user_id and old.name = ne.name and old.currency_id='DIAMOND' and ne.currency='DIAMOND' and old.balance!=ne.balance


// 按照 user_id 对比 newcore 和 oldcore TICKET
select old.user_id,old.name,old.balance,ne.balance, old.balance-ne.balance from core_account old, newbank_coreaccount ne where 1=1 and old.user_id = ne.user_id and old.name = ne.name and old.currency_id='TICKET' and ne.currency='TICKET' and old.balance!=ne.balance


// 对比 newcore  和 oldcore 差额
select ne.*, ol.balance, ne.balance-ol.balance FROM (SELECT sum(balance) balance, currency FROM `newbank_coreaccount` WHERE 1 group by currency) ne, (SELECT sum(balance) balance, currency_id FROM `core_account` WHERE 1 group by currency_id) ol where ne.currency = ol.currency_id


