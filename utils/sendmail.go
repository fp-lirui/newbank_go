package utils

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"net/smtp"
	"st/config"
	"st/models"
	"strings"
)

func Sendmail(to, subject, cc, body, mailtype string) error {
	user := config.Config.EmailSetting.EmailHostUser
	password := config.Config.EmailSetting.EmailHostPassword
	host := config.Config.EmailSetting.EmailHost
	//port := config.Config.EmailSetting.EmailPort

	auth := smtp.PlainAuth("", user, password, strings.Split(host, ":")[0])
	var content_type string
	if mailtype == "html" {
		content_type = "Content-Type: text/" + mailtype + "; charset=UTF-8"
	} else {
		content_type = "Content-Type: text/plain; charset=UTF-8"
	}

	sendto := strings.Split(to, ":")
	msg := []byte("To: " + sendto[0] + "\r\nCc: " + cc + "\r\nFrom: " + user + "\r\nSubject: " + subject + "\r\n" + content_type + "\r\n\r\n" + body)
	err := smtp.SendMail(host, auth, user, sendto, msg)
	return err
}

func SendmailRefund(userId string, amount float64, gateway string, externalId string, status int) error {
	log.WithFields(log.Fields{"Userid": userId, "gateway": gateway, "externalId": externalId, "status": status}).Info("Sendmail Refund.")
	to := config.Config.EmailSetting.To
	cc := config.Config.EmailSetting.Cc
	var subject string
	if userId != "" {
		subject = fmt.Sprintf("用户:%s 有一笔退款", userId)
	} else {
		subject = fmt.Sprintf("有笔退款操作: %s", externalId)
	}

	body := ""
	if status == 6 {
		body = fmt.Sprintf("用户:%s 有一笔退款, 金额%v, 充值平台:%s, 订单号:%s, Suffice Account。", userId, amount, gateway, externalId)
	} else if status == 5 {
		body = fmt.Sprintf("用户:%s 有一笔退款, 金额%v, 充值平台:%s, 订单号:%s, Bank没有自动扣币， 请确认。", userId, amount, gateway, externalId)
	} else if status == 4 {
		body = fmt.Sprintf("订单号:%s, Bank未找到充值记录或者充值记录已过期， 请确认。", externalId)
	} else if status == 3 {
		body = fmt.Sprintf("用户:%s 有一笔退款, 金额%v, 充值平台:%s, 订单号:%s, Bank未扣币，请确认。", userId, amount, gateway, externalId)
	} else if status == 2 {
		body = fmt.Sprintf("用户:%s 有一笔退款, 金额%v, 充值平台:%s, 订单号:%s, Bank已扣币。", userId, amount, gateway, externalId)
	} else if status == 1 {
		body = fmt.Sprintf("用户:%s 有一笔退款, 金额%v, 充值平台:%s, 订单号:%s, Bank未自动扣币，用户账户异常。", userId, amount, gateway, externalId)
	} else {
		body = fmt.Sprintf("用户:%s 有一笔退款, 金额%v, 充值平台:%s, 订单号:%s, ", userId, amount, gateway, externalId)
	}
	error := Sendmail(to, subject, cc, body, "")
	if error != nil {
		log.WithField("error", error).Error("Sendmail error.")
	}
	return error
}

func SendmailWithDepositLog(depositLog *models.GatewayDepositLog, externalId string, status int) error {
	to := "customer_service@nextentertain.com"
	cc := "rui.li@nextentertain.com"
	var subject string
	if depositLog != nil {
		subject = fmt.Sprintf("用户:%s 有一笔退款", depositLog.User_id)
	} else {
		subject = fmt.Sprintf("有笔退款操作 %s", externalId)
	}

	body := ""
	if status == 5 {
		body = fmt.Sprintf("用户:%s 有一笔退款, 金额%v, 充值平台:%s, 订单号:%s, Bank没有自动扣币， 请手动操作。", depositLog.User_id, depositLog.Amount, depositLog.Gateway, depositLog.External_id)
	} else if status == 4 {
		body = fmt.Sprintf("用户:%s 有一笔退款, 金额%v, 充值平台:%s，订单号:%s, Bank未找到充值记录或者充值记录已过期， 请手动操作。", depositLog.User_id, depositLog.Amount, depositLog.Gateway, depositLog.External_id)
	} else if status == 3 {
		body = fmt.Sprintf("用户:%s 有一笔退款, 金额%v, 充值平台:%s, 订单号:%s, ", depositLog.User_id, depositLog.Amount, depositLog.Gateway, depositLog.External_id)
	} else if status == 2 {
		body = fmt.Sprintf("用户:%s 有一笔退款, 金额%v, 充值平台:%s, 订单号:%s, Bank已扣币。", depositLog.User_id, depositLog.Amount, depositLog.Gateway, depositLog.External_id)
	} else if status == 1 {
		body = fmt.Sprintf("用户:%s 有一笔退款, 金额%v, 充值平台:%s, 订单号:%s, Bank未自动扣币，用户账户异常。", depositLog.User_id, depositLog.Amount, depositLog.Gateway, depositLog.External_id)
	} else {
		body = fmt.Sprintf("用户:%s 有一笔退款, 金额%v, 充值平台:%s, 订单号:%s, ", depositLog.User_id, depositLog.Amount, depositLog.Gateway, depositLog.External_id)
	}

	return Sendmail(to, subject, cc, body, "")

}

