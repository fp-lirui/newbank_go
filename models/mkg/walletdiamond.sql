CREATE TABLE `newbank_walletdiamond_%s` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `sn` varchar(255) NOT NULL,
  `account` varchar(255) NOT NULL,
  `num` bigint(20) unsigned NOT NULL,
  `created_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  KEY `user_id` (`user_id`),
  KEY `sn` (`sn`),
  KEY `account` (`account`),
  KEY `user_id_account` (`user_id`, `account`),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;