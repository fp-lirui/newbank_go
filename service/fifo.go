package service

import (
	"fmt"
	"st/models"
	"strconv"

	"github.com/jinzhu/gorm"
)

func GetFifoAccount(cursors [2]int) ([]*models.FifoAccount, error) {
	var fifoaccounts []*models.FifoAccount

	err := models.DB.Limit(cursors[1]).Offset(cursors[0]).Where("queue_type = ?", 1).Order("created_at asc").Find(&fifoaccounts).Error
	if err != nil {
		fmt.Println("GetFifoAccount() ErrRecordNotFound")
		return nil, err
	}
	return fifoaccounts, nil
}

func FifoZero() {
	sql := `select sum(amount), sum(value), user_id, name, currency_id from fifo_fifoaccount where queue_type=1 group by user_id, name, currency_id`
	rows, err := models.DB.Raw(sql).Rows()
	defer rows.Close()
	if err != nil {
		fmt.Println("read fifo error: ", err)
		return
	}

	defer func() {
		if err := recover(); err != nil {
			fmt.Println("FifoZero() error: ", err, "!!!!!!!!!!")
			panic(err)
		}
	}()

	for rows.Next() {
		var (
			userID   string
			account  string
			currency string
			amount   float64
			value    float64
		)
		err1 := rows.Scan(&amount, &value, &userID, &account, &currency)
		if err1 != nil {
			panic(string("rows.scan() err1:" + err1.Error()))
		}

		key := userID + ":" + account

		if currency == "TICKET" {

			ticketValue := fmt.Sprintf("%s:%d", "N0", int64(amount))
			ticketValueList := make([]interface{}, 2)
			ticketValueList[0] = ticketValue

			WalletTicketMap1[key] = ticketValueList
		} else if currency == "DIAMOND" {

			diamondValue := fmt.Sprintf("%s:%d", "N0", int64(amount))
			diamondValueList := make([]interface{}, 2)
			diamondValueList[0] = diamondValue

			WalletDiamondMap1[key] = diamondValueList

		}
	}
}

func FifoNonZero() {
	sql := `select sum(amount), sum(value), user_id, name, currency_id from fifo_fifoaccount where queue_type=2 group by user_id, name, currency_id`
	rows, err := models.DB.Raw(sql).Rows()
	defer rows.Close()
	if err != nil {
		fmt.Println("FifoNonZero() read fifo error: ", err)
		return
	}

	defer func() {
		if err := recover(); err != nil {
			fmt.Println("FifoNonZero() error: ", err, "!!!!!!!!!!")
			panic(err)
		}
	}()

	for rows.Next() {
		var (
			userID   string
			account  string
			currency string
			amount   float64
			value    float64
		)
		err1 := rows.Scan(&amount, &value, &userID, &account, &currency)
		if err1 != nil {
			panic(string("FifoNonZero() rows.scan() err1:" + err1.Error()))
		}

		var sn string
		snValstr := fmt.Sprintf("initial_%d", int64(1000000000*value/amount))
		if _, ok := SnMap[snValstr]; ok {
			sn = SnMap[snValstr]
		} else {
			snConfig, err2 := models.FindSn("fifo", snValstr)
			if err2 != nil {
				if err2 == gorm.ErrRecordNotFound {
					snConfig, err2 = models.AddSnConfigWithValue("fifo", snValstr, amount, 0, "", value)
					if err2 != nil {
						fmt.Println("FifoNonZero() AddSnConfig error: ", err2)
						panic(err2)
					}
					SnMap[snValstr] = snConfig.Sn
					sn = snConfig.Sn
				} else {
					fmt.Println("FifoNonZero() FindSn1 error: ", err2)
					panic(err2)
				}
			} else {
				sn = snConfig.Sn
				SnMap[snValstr] = snConfig.Sn
			}
		}

		key := userID + ":" + account

		if currency == "TICKET" {
			if _, ok := WalletTicketMap1[key]; ok {
				ticketList := WalletTicketMap1[key]
				ticketVal := sn + ":" + strconv.FormatInt(int64(amount), 10)
				ticketList[1] = ticketVal

				WalletTicketMap1[key] = ticketList
			} else {
				ticketValue := fmt.Sprintf("%s:%d", sn, int64(amount))
				ticketValueList := make([]interface{}, 2)
				ticketValueList[1] = ticketValue

				WalletTicketMap1[key] = ticketValueList
			}
		} else if currency == "DIAMOND" {
			if _, ok := WalletDiamondMap1[key]; ok {
				diamondList := WalletDiamondMap1[key]
				diamondVal := sn + ":" + strconv.FormatInt(int64(amount), 10)
				diamondList[1] = diamondVal

				WalletDiamondMap1[key] = diamondList
			} else {
				diamondValue := fmt.Sprintf("%s:%d", sn, int64(amount))
				diamondValueList := make([]interface{}, 2)
				diamondValueList[1] = diamondValue

				WalletDiamondMap1[key] = diamondValueList
			}
		}
	}
}

func FifoAll() {
	sql := `select sum(amount), sum(value), user_id, name, currency_id from fifo_fifoaccount group by user_id, name, currency_id`
	rows, err := models.DB.Raw(sql).Rows()
	defer rows.Close()
	if err != nil {
		fmt.Println("FifoAll() read fifo error: ", err)
		return
	}

	defer func() {
		if err := recover(); err != nil {
			fmt.Println("FifoAll() error: ", err, "!!!!!!!!!!")
			panic(err)
		}
	}()

	for rows.Next() {
		var (
			userID   string
			account  string
			currency string
			amount   float64
			value    float64
		)
		err1 := rows.Scan(&amount, &value, &userID, &account, &currency)
		if err1 != nil {
			panic(string("FifoAll() rows.scan() err1:" + err1.Error()))
		}

		key := userID + ":" + account + ":" + currency
		CoreAccountMap1[key] = int64(amount)

	}
}

var WalletDiamondMap1 map[string][]interface{}
var WalletTicketMap1 map[string][]interface{}
var CoreAccountMap1 map[string]int64
var SnMap map[string]string

func SyncFifo() {
	WalletDiamondMap1 = make(map[string][]interface{})
	WalletTicketMap1 = make(map[string][]interface{})
	CoreAccountMap1 = make(map[string]int64)
	SnMap = make(map[string]string)

	FifoZero()
	fmt.Println("finish fifozero(). len(WalletDiamondMap1):", len(WalletDiamondMap1), "  len(WalletTicketMap1):", len(WalletTicketMap1), " len(CoreAccountMap1):", len(CoreAccountMap1))
	FifoNonZero()
	fmt.Println("finish fifononezero(). len(WalletDiamondMap1):", len(WalletDiamondMap1), "  len(WalletTicketMap1):", len(WalletTicketMap1), " len(CoreAccountMap1):", len(CoreAccountMap1))
	FifoAll()
	fmt.Println("finish fifoall(). len(WalletDiamondMap1):", len(WalletDiamondMap1), "  len(WalletTicketMap1):", len(WalletTicketMap1), " len(CoreAccountMap1):", len(CoreAccountMap1))
	WalletDiamondMap = WalletDiamondMap1
	WalletTicketMap = WalletTicketMap1
	CoreAccountMap = CoreAccountMap1

	fmt.Println("start SaveCoreAccountToDB(). len(CoreAccountMap): ", len(CoreAccountMap))
	SaveCoreAccountToDB()
	fmt.Println("start SaveWalletDiamondToDB(). len(WalletDiamondMap): ", len(WalletDiamondMap))
	SaveWalletDiamondToDB()
	fmt.Println("start SaveWalletTicketToDB(). len(WalletTicketMap): ", len(WalletTicketMap))
	SaveWalletTicketToDB()
}
