package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"regexp"

	"github.com/goinggo/mapstructure"
)

var jsonData map[string]interface{}

func initJSON1() {
	bytes, err := ioutil.ReadFile("./config.json")
	if err != nil {
		fmt.Println("Read Config File : ", err.Error())
		os.Exit(-1)
	}

	configStr := string(bytes[:])
	reg := regexp.MustCompile(`/\*.*\*/`)

	configStr = reg.ReplaceAllString(configStr, "")
	bytes = []byte(configStr)
	if err := json.Unmarshal(bytes, &jsonData); err != nil {
		fmt.Println("Invalid Config File : ", err.Error())
		os.Exit(-1)
	}
}

type GoogleOAuth struct {
	ClientID     string
	SecurtKey    string
	AccessToken  string
	RefreshToken string
	Keys		 string
}

type Checkout struct {
	SECRETKEY string `json:"SECRET_KEY"`
}

type Configure struct {
	Mysql struct {
		Host          string
		ReadOnlyHost  string
		Db            string
		Password      string
		User          string
		Type          string
		Tableprefix   string
		Port          int
	}
	Dbbak struct {
		Filepath    string
		Tablesplice int
	}
	RedisSev struct {
		Addr     string
		Port     string
		Password string
		Db       string
	}
	Funbank struct{
		BaseUrl string
		PrivatePem string
	}
	GoogleAPP struct {
		JP GoogleOAuth
		Default GoogleOAuth
		Inke GoogleOAuth
	}
	Finance struct {
		Checkout Checkout
	}
	Redshift struct {
		Username string
		Password string
		Host     string
		Port     string
		DbName   string
	}
	EmailSetting struct {
		EmailHost string
		EmailPort int
		EmailHostUser string
		EmailHostPassword string
		EmailUseTls bool
		ServerEmail string
		To string
		Cc string
	}

}

var Config Configure

type dbConfig struct {
	Dialect         string
	Database        string
	User            string
	Password        string
	Host            string
	Port            int
	Charset         string
	URL             string
	MaxIdleConns    int
	MaxOpenConns    int
	ConnMaxLifetime int64
	Sslmode         string
}

var DBConfig dbConfig

func InitDB() {
	var ENVIRONMENT string
	ENVIRONMENT = os.Getenv("ENVIRONMENT")

	if ENVIRONMENT == "" {
		ENVIRONMENT = "local"
		//panic("can't found environment variable.")
	}

	file, _ := os.Open("config/config-" + ENVIRONMENT + ".json")
	defer file.Close()

	decoder := json.NewDecoder(file)

	err := decoder.Decode(&Config)

	InitEnvironment()

	if err != nil {
		fmt.Println("Error:", err)
	}

	fmt.Println("mysql :" + Config.Mysql.Host)
	fmt.Println("mysql readonly :" + Config.Mysql.Host)
	fmt.Println("mysql password:" + Config.Mysql.Password)
}

func InitEnvironment() {
	DB_NAME := os.Getenv("DB_NAME")
	DB_HOST := os.Getenv("DB_HOST")
	DB_READONLYHOST := os.Getenv("DB_READONLY_HOST")
	DB_USER := os.Getenv("DB_USER")
	DB_PASSWORD := os.Getenv("DB_PASSWORD")

	if DB_HOST != "" {
		Config.Mysql.Host = DB_HOST
	}

	if DB_READONLYHOST != "" {
		Config.Mysql.ReadOnlyHost = DB_READONLYHOST
	}

	if DB_USER != ""{
		Config.Mysql.User = DB_USER
	}

	if DB_PASSWORD != "" {
		Config.Mysql.Password = DB_PASSWORD
	}

	if DB_NAME != "" {
		Config.Mysql.Db = DB_NAME
	}
}

func initDB1() {
	if err := mapstructure.Decode(jsonData, &DBConfig); err != nil {
		panic(err)
	}

	url := fmt.Sprintf("host=%s port=%d user=%s dbname=%s password=%s sslmode=%s",
		DBConfig.Host,
		DBConfig.Port,
		DBConfig.User,
		DBConfig.Database,
		DBConfig.Password,
		DBConfig.Sslmode)

	DBConfig.URL = url
}

func init() {
	InitDB()
}
