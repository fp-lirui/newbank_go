package models

import (
	log "github.com/sirupsen/logrus"
	"time"
)

type GatewayAppleNotification struct {
	Id                  int64
	TransactionId       string
	ProductId           string
	CancellationReason  string
	CancellationAt      int64
	NotifiactionType    string
	Memo		        string
	Status      	    int
	CreatedAt           int64
}

func (t GatewayAppleNotification) TableName() string {
	return "gateway_appleiap_notifiaction"
}

type GatewayGoogleplayVoidedpurchase struct {
	Id int
	OrderId  string  `gorm:"column:order_id;not null;unique"`
	PurchaseToken string
	PurchaseTimeMillis int64
	VoidedReason int64
	VoidedSource int64
	VoidedTimeMillis int64
	Status int
	CreatedAt int64
}

func (t GatewayGoogleplayVoidedpurchase) TableName() string {
	return "gateway_googleplay_voidedpurchase"
}

func FindAppleNotification() ([]*GatewayAppleNotification, error) {
	var appleNotifiaction []*GatewayAppleNotification

	err := DBReadonly.Where("status = ?", 0).Where("created_at > ?", time.Now().Unix() - 30*24*60*60).Find(&appleNotifiaction).Error

	return appleNotifiaction, err
}

func FindAppleNotificationWithTransactionId(transactionId string) (GatewayAppleNotification, error) {
	var appleNotifiaction GatewayAppleNotification

	err := DBReadonly.Where("transaction_id = ?", transactionId).Find(&appleNotifiaction).Error

	return appleNotifiaction, err
}

func UpdateAppleNotificationStatus(transid string, status int) error {
	appleNotifiaction := &GatewayAppleNotification{}
	err:= DB.Model(appleNotifiaction).Where("transaction_id=?", transid).UpdateColumn("status", status).Error
	if err!= nil {
		return err
	}

	return nil
}

func SaveVoidedPurchase(OrderId string, PurchaseToken string, PurchaseTimeMillis int64, VoidedReason int64, VoidedSource int64, VoidedTimeMillis int64) (*GatewayGoogleplayVoidedpurchase, error) {
	voidPurchase := &GatewayGoogleplayVoidedpurchase{OrderId: OrderId, PurchaseTimeMillis: PurchaseTimeMillis,
		PurchaseToken: PurchaseToken, VoidedTimeMillis: VoidedTimeMillis, VoidedSource: VoidedSource, VoidedReason: VoidedReason,
		CreatedAt: time.Now().Unix()}
	err := DB.Model(voidPurchase).Save(voidPurchase).Error
	if err != nil {
		return voidPurchase, err
	}

	return voidPurchase, nil
}

func FindGoogleVoidedPurchases() ([]*GatewayGoogleplayVoidedpurchase, error) {
	var voidedPurchases []*GatewayGoogleplayVoidedpurchase

	err := DBReadonly.Where("status=?", "0").Where("created_at >= ?", time.Now().Unix() - 48*60*60).Order("id").Find(&voidedPurchases).Error
	if err != nil {
		return nil, err
	}

	return voidedPurchases, nil
}

func UpdateGoogleVoidedPurchasesStatus(orderId string, status int) error {
	voidedPurchase := &GatewayGoogleplayVoidedpurchase{}
	err := DB.Model(voidedPurchase).Where("order_id = ?", orderId).UpdateColumn("status", status).Error
	if err != nil {
		log.WithFields(log.Fields{"order_id": orderId}).Error("UpdateGoogleVoidedPurchasesStatus err.", err)
		return err
	}
	return nil
}