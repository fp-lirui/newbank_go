package service

import (
	"bytes"
	"fmt"
	"github.com/jinzhu/gorm"
	jsoniter "github.com/json-iterator/go"
	"io/ioutil"
	"net/http"
	"st/config"
	models "st/models"
	utils "st/utils"
	"strconv"
	"time"
)

func GetTransaction(cursors [2]int64) ([]*models.Transaction, error) {
	var transaction []*models.Transaction

	// fmt.Println("GetTransaction() walletDataConfig cursor: ", cursors)

	err := models.DB.Where("created_at >= ? AND created_at < ?", cursors[0], cursors[1]).Order("created_at asc").Find(&transaction).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		fmt.Println("GetTransaction() ErrRecordNotFound")
		return nil, err
	}

	return transaction, nil
}

type TransactionResponseAffectedAccount struct {
	UserId string
	Account string
	Currency string
	BalanceDelta string
}

type TransactionResponseTransaction struct {
	Amount string
	ConsumeType string `json:"consumed_type"`
	Type string
	DebitUser string `json:"debit_user"`
	DebitAccount string `json:"debit_account"`
	CreditUser string `json:"credit_suer"`
	CreditAccount string `json:"credit_account"`
	ExternalID string `json:"external_id"`
	CreatedAt int64 `json:"created_at"`
}

type TransactionResponseBody struct {
	ExternalId string `json:"external_id"`
	TransId string `json:"trans_id"`
	CreatedAt int64 `json:"created_at"`
	Type string `json:"type"`
	AffectedAccounts []TransactionResponseAffectedAccount `json:"affected_accounts"`
	Transaction TransactionResponseTransaction `json:"transaction"`
}

type TransactionResponse struct {
	Code int
	Reason string
	Body TransactionResponseBody
}

func BankTransaction(type1 string, external_id string, debit_user string, debit_account string, credit_user string, credit_account string, currency string, amount string) (string, error){
	url := fmt.Sprintf("%s/api/v1/bank/transaction/", config.Config.Funbank.BaseUrl)
	jsonBody := make(map[string]interface{})
	jsonBody["type"] = type1
	jsonBody["external_id"] = external_id
	jsonBody["debit_account"] = debit_account
	jsonBody["debit_user"] = debit_user
	jsonBody["credit_user"] = credit_user
	jsonBody["credit_account"] = credit_account
	jsonBody["currency"] = "DIAMOND"
	jsonBody["amount"] = amount
	jsonBody["consume_type"] = "66"

	jsonStr, _ := jsoniter.Marshal(jsonBody)
	
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	if err!= nil {
		return "", err
	}
	timestamp := time.Now().Unix()
	signuare, _ := utils.FunbankSignature("v1", strconv.FormatInt(timestamp, 10),"go", "/api/v1/bank/transaction/", "", string(jsonStr[:]))
	req.Header.Set("Funbank-Signature", signuare)
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{Timeout: 10*time.Second}

	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	fmt.Println("response Status:", resp.Status)
	fmt.Println("response Headers:", resp.Header)
	body, err := ioutil.ReadAll(resp.Body)
	fmt.Println("response Body:", string(body))
	return string(body), err
}
