package models

func GetUserAccount(userid string, name string) (*CoreAccount, error) {
	userAccount := CoreAccount{UserId: userid, Name: name}
	err := DBReadonly.Where("user_id=? and name=? and currency='DIAMOND'", userid, name).Find(&userAccount).Error

	if err != nil {
		return nil, err
	}

	return &userAccount, nil
}
