package models

type Transaction struct {
	// gorm.Model
	Id           string
	ExternalId   string
	DebitUserId  string
	CreditUserId string
	Blob         string
	Type         int
	CreatedAt    int
}

func (t Transaction) TableName() string {
	return "core_transaction"
}
