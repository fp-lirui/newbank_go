package service

import (
	"errors"
	"fmt"
	"github.com/jinzhu/gorm"
	jsoniter "github.com/json-iterator/go"
	"github.com/json-iterator/go/extra"
	log "github.com/sirupsen/logrus"
	"st/models"
	"st/utils"
	"strconv"
	"sync"
	"context"
	"time"
)

func GoogleOrder() error {
	return OrderChecker()
}

func RefundOrderIap() error {
	log.Info("RefundOrderIap start!")
	appleNotification, err := models.FindAppleNotification()
	if err != nil {
		log.WithFields(log.Fields{"err": err}).Error("FindAppleNotification")
		return err
	}
	log.WithField("count(depositLogs)", len(appleNotification)).Info("RefundOrderIap get appleNotification:")
	chan_deposit := make(chan int)
	var wg sync.WaitGroup
	chan_deposit1 := make(chan *models.GatewayDepositLog, 10)
	go func() {
		log.Info("RefundOrderIap range depositlogs.")
		for _, value := range appleNotification {
			var refundChecker = &AppleChecker{}

			ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
			depositLog, err := refundChecker.checkOrderWithCtx(ctx, value)
			if err!= nil && errors.Is(err, gorm.ErrRecordNotFound) {
				log.WithFields(log.Fields{"trans_id": value.TransactionId}).Error("refundChecker checkOrderWithCtx error ErrRecordNotFound in Month.", err)
				depositLog, err = refundChecker.checkOrderWithCtxInYear(ctx, value)
			}
			if err != nil {
				if errors.Is(err, gorm.ErrRecordNotFound) {
					log.WithFields(log.Fields{"trans_id": value.TransactionId}).Error("refundChecker checkOrderWithCtx error ErrRecordNotFound in Year.", err)
					utils.SendmailRefund("", 0, "", fmt.Sprintf("appleiap.%s", value.TransactionId), 4)
					var refundChecker IRefundChecker
					refundChecker = &AppleChecker{}
					err3 := refundChecker.proccessExternalId(value.TransactionId, 4)
					if err3 != nil {
						log.WithFields(log.Fields{"trans_id": value.TransactionId}).Error("refundChecker proccessExternalId error ", err3)
					}
				} else {
					log.WithFields(log.Fields{"err": err}).Error("refundChecker checkOrderWithCtx error")
				}

			} else {
				log.WithFields(log.Fields{"state": depositLog.Refunded, "id": value.Id}).Info("refundChecker checkOrderWithCtx")
				if depositLog.Refunded == 0 {
					wg.Add(1)
					chan_deposit1 <- depositLog
				} else {
					wg.Add(1)
					chan_deposit1 <- depositLog
					//utils.SendmailRefund(depositLog.User_id, depositLog.Amount, depositLog.Gateway, depositLog.External_id, 5)
					//var refundChecker IRefundChecker
					//refundChecker = &AppleChecker{}
					//err3 := refundChecker.proccessExternalId(value.TransactionId, 5)
					//if err3 != nil {
					//	log.WithFields(log.Fields{"trans_id": value.TransactionId}).Error("refundChecker checkOrderWithCtx error ErrRecordNotFound.", err3)
					//}
				}
			}
		}
		chan_deposit <- 1
		close(chan_deposit1)
		close(chan_deposit)
	}()

	go func() {
		for value := range chan_deposit1 {
			func() {
				log.WithField("extelnal_id", value.External_id).Info("RefundOrderIap make banktransaction")
				external_id := value.External_id
				debit_user := value.User_id
				credit_user := value.User_id
				gateway := value.Gateway
				amount1 := value.Amount

				debit_account := "default"
				credit_account := "refund_locked"

				defer wg.Done()

				var bundle map[string]string
				var json_iterator = jsoniter.ConfigCompatibleWithStandardLibrary
				error := json_iterator.Unmarshal([]byte(value.Bundle_content), &bundle)
				if error != nil {
					panic(error)
				}
				amount := bundle["DIAMOND"]

				refund_external_id := fmt.Sprintf("%s_refund", external_id)

				user, err := models.GetUserAccount(debit_user, debit_account)
				if err != nil {
					if errors.Is(err, gorm.ErrRecordNotFound) {
						utils.SendmailRefund(debit_user, amount1, gateway, external_id, 1)
						AddUserRisks(debit_user, 0, 0, 1)
						var refundChecker IRefundChecker
						refundChecker = &AppleChecker{}
						err3 := refundChecker.proccess(value, 1)
						if err3 != nil {
							log.WithFields(log.Fields{"refunded_external_id": value.External_id}).Error("GetUserAccount proccessExternalId error.", err3)
						}
					} else {
						log.WithFields(log.Fields{"debit_user": debit_user, "debit_account": debit_account}).Error("GetUserAccount error")
					}
					return
				}

				b_amount, err := strconv.ParseInt(amount, 10, 64)
				if err != nil {
					panic(err)
				}
				if user.Balance < b_amount {
					// 不够
					amount = fmt.Sprintf("%d", user.Balance)
				}

				amount0, amount_err := strconv.ParseInt(amount, 10, 64)
				if  amount0 == 0 || amount_err != nil {
					utils.SendmailRefund(debit_user, amount1, gateway, external_id, 1)
					AddUserRisks(debit_user, 0, 0, 1)
					if amount_err != nil {
						log.WithFields(log.Fields{"amount": amount0, "user": debit_user, "external_id": external_id}).Error("Amount parseInt err", amount_err)
					} else {
						log.WithFields(log.Fields{"amount": amount0, "user": debit_user, "external_id": external_id}).Info("Not need to make trans")
					}
					var refundChecker IRefundChecker
					refundChecker = &AppleChecker{}
					err3 := refundChecker.proccess(value, 1)
					if err3 != nil {
						log.WithFields(log.Fields{"refunded_external_id": value.External_id}).Error("AppleChecker proccess error.", err3)
					}
				} else {

					resp, err := BankTransaction("transfer", refund_external_id, debit_user, debit_account, credit_user, credit_account, "DIAMOND", amount);
					if err != nil {
						log.WithFields(log.Fields{"type": "transfer", "debit_user": debit_user, "debit_account": debit_account, "credit_user": credit_user, "credit_account": credit_account, "amount": amount}).Error("BankTransaction err;", err)
					} else {
						var respJson TransactionResponse
						extra.RegisterFuzzyDecoders()
						var json_iterator = jsoniter.ConfigCompatibleWithStandardLibrary
						json_iterator.Unmarshal([]byte(resp), &respJson)
						code := respJson.Code
						reason := respJson.Reason
						bodyJson := respJson.Body
						refunded_external_id := bodyJson.ExternalId
						refunded_trans_id := bodyJson.TransId

						if (code == 0 || code == 3001) && reason == "Success" {
							var status int
							if code == 0 {
								status = 2
								AddUserRisks(debit_user, 1, 0, 0)
							} else {
								status = 6
								AddUserRisks(debit_user, 0, 1, 0)
							}

							log.WithFields(log.Fields{"response": resp, "code": code, "reason": reason, "body": bodyJson, "refunded_external_id": refunded_external_id, "refunded_trans_id": refunded_trans_id}).Info("bank response.")
							utils.SendmailRefund(debit_user, amount1, gateway, external_id, status)
							err1 := models.UpdateDepositLogRefunded(value.ID)
							err2 := models.CreateDepositLogRefund(debit_user, gateway, value.ID, refunded_external_id, refunded_trans_id, b_amount, user.Balance)

							var refundChecker = &AppleChecker{}

							err3 := refundChecker.proccess(value, status)

							if err1 != nil {
								log.WithFields(log.Fields{"response": resp, "code": code, "reason": reason, "body": bodyJson, "refunded_external_id": refunded_external_id, "refunded_trans_id": refunded_trans_id}).Error("bank response. UpdateDepositLogRefunded", err1)
							}
							if err2 != nil {
								log.WithFields(log.Fields{"response": resp, "code": code, "reason": reason, "body": bodyJson, "refunded_external_id": refunded_external_id, "refunded_trans_id": refunded_trans_id}).Error("bank response. CreateDepositLogRefund", err2)
							}
							if err3 != nil {
								log.WithFields(log.Fields{"response": resp, "code": code, "reason": reason, "body": bodyJson, "refunded_external_id": refunded_external_id, "refunded_trans_id": refunded_trans_id}).Error("bank response. proccess", err3)
							}
						} else if (code == 3006) {
							utils.SendmailRefund(debit_user, amount1, gateway, external_id, 3)
							err1 := models.UpdateDepositLogRefunded(value.ID)
							var refundChecker = &AppleChecker{}

							err3 := refundChecker.proccess(value, 3)
							if err1 != nil {
								log.WithFields(log.Fields{"response": resp, "code": code, "reason": reason, "body": bodyJson, "refunded_external_id": refunded_external_id, "refunded_trans_id": refunded_trans_id}).Error("bank response. UpdateDepositLogRefunded", err1)
							}
							if err3 != nil {
								log.WithFields(log.Fields{"response": resp, "code": code, "reason": reason, "body": bodyJson, "refunded_external_id": refunded_external_id, "refunded_trans_id": refunded_trans_id}).Error("bank response. proccess", err3)
							}
						} else {
							//panic(fmt.Sprintf("bank response error, code:%s reason:%s", code, reason))
							log.WithFields(log.Fields{"response": resp, "code": code, "reason": reason, "body": bodyJson, "refund_external_id": refund_external_id}).Error("bank response error.")
						}

					}
				}
			}()

		}
	}()

	<-chan_deposit

	wg.Wait()
	log.Info("RefundOrderIap done!")

	return nil
}


func RefundOrderIab() error {
	// google 退款处理
	// 遍历google退款记录表 gateway_googleplayiab_voidedpurchase
	log.Info("RefundOrder start!")
	voidedPurchases, err := models.FindGoogleVoidedPurchases()
	if err != nil {
		log.WithFields(log.Fields{"err": err}).Error("FindGoogleVoidedPurchases")
		return err
	}
	log.WithField("count(voidedPurchases)", len(voidedPurchases)).Info("RefundOrderIab get voidedPurchases:")
	chan_deposit := make(chan int)
	var wg sync.WaitGroup
	chan_deposit1 := make(chan *models.GatewayDepositLog, 10)
	go func() {
		log.Info("RefundOrder range voidedPurchases.")
		for _, value := range voidedPurchases {
			//ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
			depositLog, err := models.FindDepositLogWithExternalId(fmt.Sprintf("googleplayiab.%s", value.OrderId))
			if err!= nil && errors.Is(err, gorm.ErrRecordNotFound) {
				log.WithFields(log.Fields{"refunded_external_id": value.OrderId}).Error("FindDepositLogWithExternalId error ErrRecordNotFound in Month.", err)
				depositLog, err = models.FindDepositLogWithExternalIdInYear(fmt.Sprintf("googleplayiab.%s", value.OrderId))
			}
			if err != nil {
				if errors.Is(err, gorm.ErrRecordNotFound) {
					log.WithFields(log.Fields{"refunded_external_id": value.OrderId}).Error("FindDepositLogWithExternalId error ErrRecordNotFound in Year.", err)
					utils.SendmailRefund("", 0, "", fmt.Sprintf("googleplayiab.%s", value.OrderId), 4)
					var refundChecker IRefundChecker
					refundChecker = &GoogleChecker{}
					err3 := refundChecker.proccessExternalId(value.OrderId, 4)
					if err3 != nil {
						log.WithFields(log.Fields{"refunded_external_id": value.OrderId}).Error("refundChecker proccessExternalId error ", err3)
					}
				} else {
					log.WithFields(log.Fields{"err": err}).Error("FindDepositLogWithExternalId error")
				}
			} else {
				log.WithFields(log.Fields{"external_id": depositLog.External_id, "amount": depositLog.Amount, "user_id": depositLog.User_id, "Refunded": depositLog.Refunded}).Info("FindDepositLogWithExternalId Log:")
				if depositLog.Refunded == 0 {
					wg.Add(1)
					chan_deposit1 <- depositLog
				} else {
					wg.Add(1)
					chan_deposit1 <- depositLog
					//utils.SendmailRefund("", 0, "", fmt.Sprintf("googleplayiab.%s", value.OrderId), 5)
					//var refundChecker IRefundChecker
					//refundChecker = &GoogleChecker{}
					//err3 := refundChecker.proccessExternalId(value.OrderId, 5)
					//if err3 != nil {
					//	log.WithFields(log.Fields{"refunded_external_id": value.OrderId}).Error("FindDepositLogWithExternalId error ErrRecordNotFound.", err3)
					//}
				}
			}
		}
		chan_deposit <- 1
		close(chan_deposit1)
		close(chan_deposit)
	}()

	go func() {
		for value := range chan_deposit1 {
			func() {
				log.WithField("external_id", value.External_id).Info("RefundOrderIab make banktransaction")
				external_id := value.External_id
				debit_user := value.User_id
				credit_user := value.User_id
				gateway := value.Gateway
				amount1 := value.Amount

				debit_account := "default"
				credit_account := "refund_locked"

				defer wg.Done()

				var bundle map[string]string
				var json_iterator = jsoniter.ConfigCompatibleWithStandardLibrary
				error := json_iterator.Unmarshal([]byte(value.Bundle_content), &bundle)
				if error != nil {
					log.WithFields(log.Fields{"bundle": value.Bundle_content}).Error("json_iterator.Unmarshal err;", error)
				} else {

					amount := bundle["DIAMOND"]

					refund_external_id := fmt.Sprintf("%s_refund", external_id)

					user, err := models.GetUserAccount(debit_user, debit_account)
					if err != nil {
						if errors.Is(err, gorm.ErrRecordNotFound) {
							utils.SendmailRefund(debit_user, amount1, gateway, external_id, 1)
							AddUserRisks(debit_user, 0, 0, 1)
							var refundChecker IRefundChecker
							refundChecker = &GoogleChecker{}
							err3 := refundChecker.proccess(value, 1)
							if err3 != nil {
								log.WithFields(log.Fields{"refunded_external_id": value.External_id}).Error("GetUserAccount proccessExternalId error.", err3)
							}
						} else {
							log.WithFields(log.Fields{"debit_user": debit_user, "debit_account": debit_account}).Error("GetUserAccount error")
						}
						return
					}

					b_amount, err := strconv.ParseInt(amount, 10, 64)
					if err != nil {
						log.WithFields(log.Fields{"amount": amount}).Error("ParseInt err;", err)
						return
					}
					if user.Balance < b_amount {
						// 不够
						amount = fmt.Sprintf("%d", user.Balance)
					}

					amount0, amount_err := strconv.ParseInt(amount, 10, 64)
					if  amount0 == 0 || amount_err != nil {
						utils.SendmailRefund(debit_user, amount1, gateway, external_id, 1)
						AddUserRisks(debit_user, 0, 0, 1)
						if amount_err != nil {
							log.WithFields(log.Fields{"amount": amount0, "user": debit_user, "external_id": external_id}).Error("Amount parseInt err", amount_err)
						} else {
							log.WithFields(log.Fields{"amount": amount0, "user": debit_user, "external_id": external_id}).Info("Not need to make trans")
						}
						var refundChecker IRefundChecker
						refundChecker = &GoogleChecker{}
						err3 := refundChecker.proccess(value, 1)
						if err3 != nil {
							log.WithFields(log.Fields{"external_id": value.External_id}).Error("GoogleChecker proccess error.", err3)
						}
					} else {

						resp, err := BankTransaction("transfer", refund_external_id, debit_user, debit_account, credit_user, credit_account, "DIAMOND", amount)
						if err != nil {
							log.WithFields(log.Fields{"type": "transfer", "debit_user": debit_user, "debit_account": debit_account, "credit_user": credit_user, "credit_account": credit_account, "amount": amount}).Error("BankTransaction err;", err)
						} else {
							var respJson TransactionResponse
							extra.RegisterFuzzyDecoders()
							var json_iterator = jsoniter.ConfigCompatibleWithStandardLibrary
							json_iterator.Unmarshal([]byte(resp), &respJson)
							code := respJson.Code
							reason := respJson.Reason
							bodyJson := respJson.Body
							refunded_external_id := bodyJson.ExternalId
							refunded_trans_id := bodyJson.TransId

							if (code == 0 || code == 3001) && reason == "Success" {
								var status int
								if code == 0 {
									status = 2
									AddUserRisks(debit_user, 1, 0, 0)
								} else {
									status = 6
									AddUserRisks(debit_user, 0, 1, 0)
								}
								log.WithFields(log.Fields{"response": resp, "code": code, "reason": reason, "body": bodyJson, "refunded_external_id": refunded_external_id, "refunded_trans_id": refunded_trans_id}).Info("bank response.")
								utils.SendmailRefund(debit_user, amount1, gateway, external_id, status)
								err1 := models.UpdateDepositLogRefunded(value.ID)
								err2 := models.CreateDepositLogRefund(debit_user, gateway, value.ID, refunded_external_id, refunded_trans_id, b_amount, user.Balance)

								var refundChecker IRefundChecker
								refundChecker = &GoogleChecker{}

								err3 := refundChecker.proccess(value, status)

								if err1 != nil {
									log.WithFields(log.Fields{"response": resp, "code": code, "reason": reason, "body": bodyJson, "refunded_external_id": refunded_external_id, "refunded_trans_id": refunded_trans_id}).Error("bank response. UpdateDepositLogRefunded", err1)
								}
								if err2 != nil {
									log.WithFields(log.Fields{"response": resp, "code": code, "reason": reason, "body": bodyJson, "refunded_external_id": refunded_external_id, "refunded_trans_id": refunded_trans_id}).Error("bank response. CreateDepositLogRefund", err2)
								}
								if err3 != nil {
									log.WithFields(log.Fields{"response": resp, "code": code, "reason": reason, "body": bodyJson, "refunded_external_id": refunded_external_id, "refunded_trans_id": refunded_trans_id}).Error("bank response. proccess", err3)
								}

							} else if (code == 3006) {
								// Duplicated transaction
								utils.SendmailRefund(debit_user, amount1, gateway, external_id, 3)
								err1 := models.UpdateDepositLogRefunded(value.ID)
								if err1 != nil {
									log.WithFields(log.Fields{"response": resp, "code": code, "reason": reason, "body": bodyJson, "refunded_external_id": refunded_external_id, "refunded_trans_id": refunded_trans_id}).Error("bank response. UpdateDepositLogRefunded", err1)
								}

								var refundChecker IRefundChecker
								refundChecker = &GoogleChecker{}
								err3 := refundChecker.proccess(value, 3)
								if err3 != nil {
									log.WithFields(log.Fields{"response": resp, "code": code, "reason": reason, "body": bodyJson, "refunded_external_id": refunded_external_id, "refunded_trans_id": refunded_trans_id}).Error("bank response. proccess", err3)
								}
							} else {
								//panic(fmt.Sprintf("bank response error, code:%s reason:%s", code, reason))
								log.WithFields(log.Fields{"response": resp, "code": code, "reason": reason, "body": bodyJson, "refund_external_id": refund_external_id}).Error("bank response error.")
							}

						}
					}
				}
			}()
		}
	}()

	<-chan_deposit

	wg.Wait()
	log.Info("RefundOrder done!")

	return nil
}


func RefundOrder() error {
	log.Info("RefundOrder start!")
	depositLogs, err := models.Find48agoDepositLogs()
	if err != nil {
		log.WithFields(log.Fields{"err": err}).Error("Find48agoDepostilLogs")
		return err
	}
	log.WithField("count(depositLogs)", len(depositLogs)).Info("RefundOrder get depositlog:")
	chan_deposit := make(chan int)
	var wg sync.WaitGroup
	chan_deposit1 := make(chan *models.GatewayDepositLog, 10)
	go func() {
		log.Info("RefundOrder range depositlogs.")
		for _, value := range depositLogs {
			gateway := value.Gateway

			var refundChecker IRefundChecker
			if gateway == "googleplayiab" {
				refundChecker = &GoogleChecker{}
			} else if gateway == "appleiap" {
				refundChecker = &AppleChecker{}
			}

			ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
			state, err := refundChecker.checkerWithCtx(ctx, value)
			if err != nil {
				log.WithFields(log.Fields{"err": err}).Error("PurchasesProductCheckerWithCredentials error")
			} else {
				log.WithFields(log.Fields{"external_id": value.External_id, "state": state, "id": value.ID}).Info("RefundOrder checker")
				value.Refunded = state
				if state > 0 {
					wg.Add(1)
					chan_deposit1 <- value
				}
			}
		}
		chan_deposit <- 1
		close(chan_deposit1)
		close(chan_deposit)
	}()

	go func() {
		for value := range chan_deposit1 {
			func() {
				log.WithField("external_id", value.External_id).Info("RefundOrder make banktransaction")
				external_id := value.External_id
				debit_user := value.User_id
				credit_user := value.User_id
				gateway := value.Gateway

				debit_account := "default"
				credit_account := "refund_locked"

				defer wg.Done()

				var bundle map[string]string
				var json_iterator = jsoniter.ConfigCompatibleWithStandardLibrary
				error := json_iterator.Unmarshal([]byte(value.Bundle_content), &bundle)
				if error != nil {
					log.WithFields(log.Fields{"bundle": value.Bundle_content}).Error("json_iterator.Unmarshal err;", error)
				} else {

					amount := bundle["DIAMOND"]

					refund_external_id := fmt.Sprintf("%s_refund", external_id)

					user, err := models.GetUserAccount(debit_user, debit_account)
					if err != nil {
						log.WithFields(log.Fields{"debit_user": debit_user, "debit_account": debit_account}).Error("GetUserAccount err;", err)
						return
					}

					b_amount, err := strconv.ParseInt(amount, 10, 64)
					if err != nil {
						log.WithFields(log.Fields{"amount": amount}).Error("ParseInt err;", err)
						return
					}
					if user.Balance < b_amount {
						// 不够
						amount = fmt.Sprintf("%d", user.Balance)
					}

					resp, err := BankTransaction("transfer", refund_external_id, debit_user, debit_account, credit_user, credit_account, "DIAMOND", amount);
					if err != nil {
						log.WithFields(log.Fields{"type": "transfer", "debit_user": debit_user, "debit_account": debit_account, "credit_user": credit_user, "credit_account": credit_account, "amount": amount}).Error("BankTransaction err;", err)
					} else {
						var respJson TransactionResponse
						extra.RegisterFuzzyDecoders()
						var json_iterator = jsoniter.ConfigCompatibleWithStandardLibrary
						json_iterator.Unmarshal([]byte(resp), &respJson)
						code := respJson.Code
						reason := respJson.Reason
						bodyJson := respJson.Body
						refunded_external_id := bodyJson.ExternalId
						refunded_trans_id := bodyJson.TransId

						if (code == 0 || code == 3001) && reason == "Success" {
							log.WithFields(log.Fields{"response": resp, "code": code, "reason": reason, "body": bodyJson, "refunded_external_id": refunded_external_id, "refunded_trans_id": refunded_trans_id}).Info("bank response.")
							err1 := models.UpdateDepositLogRefunded(value.ID)
							err2 := models.CreateDepositLogRefund(debit_user, gateway, value.ID, refunded_external_id, refunded_trans_id, b_amount, user.Balance)

							var refundChecker IRefundChecker
							if gateway == "googleplayiab" {
								refundChecker = &GoogleChecker{}
							} else if gateway == "appleiap" {
								refundChecker = &AppleChecker{}
							}
							err3 := refundChecker.proccess(value, 2)

							if err1 != nil {
								log.WithFields(log.Fields{"response": resp, "code": code, "reason": reason, "body": bodyJson, "refunded_external_id": refunded_external_id, "refunded_trans_id": refunded_trans_id}).Error("bank response. UpdateDepositLogRefunded", err1)
							}
							if err2 != nil {
								log.WithFields(log.Fields{"response": resp, "code": code, "reason": reason, "body": bodyJson, "refunded_external_id": refunded_external_id, "refunded_trans_id": refunded_trans_id}).Error("bank response. CreateDepositLogRefund", err2)
							}
							if err2 != nil {
								log.WithFields(log.Fields{"response": resp, "code": code, "reason": reason, "body": bodyJson, "refunded_external_id": refunded_external_id, "refunded_trans_id": refunded_trans_id}).Error("bank response. proccess", err3)
							}
						} else {
							//panic(fmt.Sprintf("bank response error, code:%s reason:%s", code, reason))
							log.WithFields(log.Fields{"response": resp, "code": code, "reason": reason, "body": bodyJson, "refund_external_id": refund_external_id}).Error("bank response error.")
						}

					}
				}
			}()
		}
	}()

	<-chan_deposit

	wg.Wait()
	log.Info("RefundOrder done!")

	return nil
}
