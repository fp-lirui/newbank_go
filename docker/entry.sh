#!/bin/bash -e

# checkout Google
echo "RUN CHECKOUT START"
echo "CMD: $CMD"
if [ "$CMD" == "refundinit" ];
then
  echo "GOOGLE CHECKER INIT."
  cd /mnt/
  go run main.go -refundinit
elif [ "$CMD" == "refundcheck" ];
then
  echo "GOOGLE CHECKER"
  cd /mnt/
  go run main.go -refundcheck
fi
echo "RUN CHECKOUT DONE"