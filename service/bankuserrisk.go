package service

import (
	"fmt"
	"github.com/jinzhu/gorm"
	log "github.com/sirupsen/logrus"
	"st/models"
	refunderror "st/utils/error"
)

func FindUserRisks(uid string)(*models.BankUserrisks, error) {
	if len(uid) <= 0 {
		return nil, refunderror.INVALID_PARAMETER_ERROR
	}

	var bankuserrisks models.BankUserrisks
	err := models.DB.Where("uid = ?", uid).Take(&bankuserrisks).Error
	if err != nil && err == gorm.ErrRecordNotFound {
		fmt.Println("FindUserRisks() ErrRecordNotFound")
		return nil, refunderror.NOT_EXIST_ENTIES_ERROR
	}

	return &bankuserrisks, nil
}

func AddUserRisks(uid string, moneyback int, sufficeaccount int, nullaccount int)(*models.BankUserrisks, error) {
	log.WithFields(log.Fields{"uid:": uid}).Info("AddUserRisks")
	bankuserrisks, err := FindUserRisks(uid)
	if err != nil && err == refunderror.INVALID_PARAMETER_ERROR {
		log.WithFields(log.Fields{"error:": refunderror.INVALID_PARAMETER_ERROR.Error()}).Error("AddUserRisks")
		return nil, err
	}

	if err != nil && err == refunderror.NOT_EXIST_ENTIES_ERROR {
		return models.SaveBankUserRisks(uid, moneyback, sufficeaccount, nullaccount)
	}

	err = models.UpdateBankUserRisk(bankuserrisks, moneyback, sufficeaccount, nullaccount)
	if err != nil {
		log.WithFields(log.Fields{"bankuserrisks:": bankuserrisks.ID}).Error("AddUserRisks")
	}

	return bankuserrisks, err
}

func AddUserRisksInsufficient(uid string)(*models.BankUserrisks, error) {
	bankuserrisks, err := FindUserRisks(uid)
	if err != nil && err == refunderror.INVALID_PARAMETER_ERROR {
		log.WithFields(log.Fields{"error:": refunderror.INVALID_PARAMETER_ERROR.Error()}).Error("AddUserRisksInsufficient")
		return nil, err
	}

	if err != nil && err == refunderror.NOT_EXIST_ENTIES_ERROR {
		return models.SaveBankUserRisks(uid, 0, 1, 0)
	}

	err = models.UpdateBankUserRisk(bankuserrisks, 0, 1, 0)
	if err != nil {
		log.WithFields(log.Fields{"bankuserrisks:": bankuserrisks.ID}).Error("AddUserRisksInsufficient")
	}

	return bankuserrisks, err
}

func AddUserRisksNullaccount(uid string)(*models.BankUserrisks, error) {
	bankuserrisks, err := FindUserRisks(uid)
	if err != nil && err == refunderror.INVALID_PARAMETER_ERROR {
		log.WithFields(log.Fields{"error:": refunderror.INVALID_PARAMETER_ERROR.Error()}).Error("AddUserRisksNullaccount")
		return nil, err
	}

	if err != nil && err == refunderror.NOT_EXIST_ENTIES_ERROR {
		return models.SaveBankUserRisks(uid, 0, 0,1)
	}

	err = models.UpdateBankUserRisk(bankuserrisks, 0, 0, 1)
	if err != nil {
		log.WithFields(log.Fields{"bankuserrisks:": bankuserrisks.ID}).Error("AddUserRisksNullaccount")
	}

	return bankuserrisks, err
}

func AddUserRisksMoneyback(uid string)(*models.BankUserrisks, error) {
	bankuserrisks, err := FindUserRisks(uid)
	if err != nil && err == refunderror.INVALID_PARAMETER_ERROR {
		log.WithFields(log.Fields{"error:": refunderror.INVALID_PARAMETER_ERROR.Error()}).Error("AddUserRisksMoneyback")
		return nil, err
	}

	if err != nil && err == refunderror.NOT_EXIST_ENTIES_ERROR {
		return models.SaveBankUserRisks(uid, 1, 0, 0)
	}

	err = models.UpdateBankUserRisk(bankuserrisks, 1, 0, 0)
	if err != nil {
		log.WithFields(log.Fields{"bankuserrisks:": bankuserrisks.ID}).Error("AddUserRisksMoneyback")
	}

	return bankuserrisks, err
}