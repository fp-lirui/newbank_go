package models

import (
	"github.com/jinzhu/gorm"
	log "github.com/sirupsen/logrus"
	"time"
)

type BankUserrisks struct {
	ID int `gorm:"primary_key"`
	Uid string `gorm:"type:varchar(50);uniqueIndex;not null;"`
	RefundMoneybackCount int `gorm:"type:int;default:0"`
	RefundSufficeaccountCount int `gorm:"type:int;default:0"`
	RefundNullaccountCount int `gorm:"type:int;default:0"`
	Risks int `gorm:"type:int;default:0"`
	CreatedAt int64 `gorm:"type:bigint"`
	LastedAt int64 `gorm:"type:bigint"`
	IsActive int `gorm:"type:int;default:0"`
	IsRelate int `gorm:"type:int;default:0"`
}

func SaveBankUserRisks(uid string, moneyback int, sufficeaccount int, nullaccount int) (*BankUserrisks, error) {
	bankUserRisk := &BankUserrisks{Uid: uid, RefundMoneybackCount: moneyback, RefundSufficeaccountCount: sufficeaccount, RefundNullaccountCount: nullaccount, Risks: 0, CreatedAt: time.Now().Unix(),
		LastedAt: time.Now().Unix(), IsActive: 0, IsRelate: 0}
	err := DB.Model(bankUserRisk).Create(bankUserRisk).Error
	if err != nil {
		log.WithFields(log.Fields{"error": err}).Error("SaveBankUserRisks.")
		return nil, err
	}

	return bankUserRisk, nil
}

func UpdateBankUserRiskWithUid(uid string, moneyback int, sufficeaccount int, nullaccount int) error {

	bankUserRisk := &BankUserrisks{}
	err := DB.Where("uid= ?", uid).First(bankUserRisk).Error
	if err != nil && err == gorm.ErrRecordNotFound{
		_, err = SaveBankUserRisks(uid, moneyback, sufficeaccount, nullaccount)
		return err
	}
	return DB.Model(bankUserRisk).Update("refund_count", gorm.Expr("refund_count + 1")).Error
}

func UpdateBankUserRisk(model *BankUserrisks, moneyback int, sufficeaccount int, nullaccount int) error {

	if moneyback > 0 {
		return DB.Model(model).Updates(map[string]interface{}{"refund_moneyback_count": gorm.Expr("refund_moneyback_count + 1"), "lasted_at":time.Now().Unix()}).Error
	}
	if sufficeaccount > 0 {
		return DB.Model(model).Updates(map[string]interface{}{"refund_sufficeaccount_count": gorm.Expr("refund_sufficeaccount_count + 1"), "lasted_at": time.Now().Unix()}).Error
	}
	if nullaccount > 0 {
		return DB.Model(model).Updates(map[string]interface{}{"refund_nullaccount_count": gorm.Expr("refund_nullaccount_count + 1"), "lasted_at": time.Now().Unix()}).Error
	}

	return nil
}
