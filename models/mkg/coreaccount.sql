CREATE TABLE `newbank_coreaccount%s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `currency` varchar(10) NOT NULL,
  `balance` bigint(20) unsigned NOT NULL,
  `created_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `name` (`name`),
  KEY `currency` (`currency`),
  UNIQUE KEY `user_id_name_currency` (`user_id`,`name`,`currency`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;