package service

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"st/config"
	models "st/models"
	"strconv"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
)

type WalletDiamond struct {
	Id      int
	UserId  string
	Sn      string
	Account string
	Num     int
}

func (w *WalletDiamond) Add() error {
	a := map[string]interface{}{
		"UserId":  w.UserId,
		"Sn":      w.Sn,
		"Account": w.Account,
		"Num":     w.Num,
	}

	if err := models.DB.Create(a).Error; err != nil {
		return err
	}

	return nil
}

func (w *WalletDiamond) FindUserWalletDiamond(user_id string) ([]*models.WalletDiamond, error) {
	var walletDiamond []*models.WalletDiamond

	err := models.DB.Where("user_id", user_id).Order("id desc").Find(&walletDiamond).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		fmt.Println("FindUserWallet() ErrRecordNotFound")
		return nil, err
	}

	return walletDiamond, nil
}

func (w *WalletDiamond) DeleteUserWalletDiamond(id int, user_id string) error {
	if err := models.DB.Where("id <= ?", id, "user_id", user_id).Delete(models.WalletDiamond{}).Error; err != nil {
		return err
	}

	return nil
}

func (w *WalletDiamond) UpdateUserWalletDiamond(id int) error {
	if err := models.DB.Model(&models.WalletDiamond{}).Where("id = ? ", id).Updates(w).Error; err != nil {
		return err
	}

	return nil
}

type WalletTicket struct {
	Id      int
	UserId  string
	Sn      string
	Account string
	Num     int
}

func (w *WalletTicket) Add() error {
	a := map[string]interface{}{
		"UserId":  w.UserId,
		"Sn":      w.Sn,
		"Account": w.Account,
		"Num":     w.Num,
	}

	if err := models.DB.Create(a).Error; err != nil {
		return err
	}

	return nil
}

var (
	walletdiamondmap map[string]interface{}
	walletticketmap  map[string]interface{}
)

type diamondValue struct {
	snnum string
}

type diamondMap struct {
	userAccount  string
	diamondValue []diamondValue
}

var WalletDiamondMap map[string][]interface{}
var WalletTicketMap map[string][]interface{}
var CoreAccountMap map[string]int64

func Readdiamondtomap() {
	/*
		{
			$uid:$name: [$sn:$num, $sn:$num, $sn:$num, ...],
			$uid:$name: [$sn:$num],
			$uid:$name: [$sn:$num]
		}
	*/
	// walletdiamondmap := make(map[string][]string, 1000)
	WalletDiamondMap = make(map[string][]interface{})

	for i := 0; i < config.Config.Dbbak.Tablesplice; i++ {

		cursor := 0
		resultsize := 3000

		var count int
		models.DB.Table(" newbank_walletdiamond_" + strconv.Itoa(i)).Model(&models.WalletDiamond{}).Count(&count)

		loopcount := count/resultsize + 1

		sql := `select id, user_id, account, sn, num from newbank_walletdiamond_` + strconv.Itoa(i) + ` order by id limit ?, ?`
		for i := 0; i < loopcount; i++ {
			// var results []WalletDiamond
			// fmt.Println("sql:", sql)
			// sql_tmp := strings.Replace(sql, "%s", strconv.Itoa(cursor), 1)
			// fmt.Println("sql:", sql_tmp)
			// sql_tmp = strings.Replace(sql_tmp, "%s", strconv.Itoa(resultsize), 1)
			// fmt.Println("sql:", sql_tmp)
			// models.DB.Row(sql_tmp)
			// rows, err := models.DB.Row().Scan()
			rows, err := models.DB.Raw(sql, cursor, resultsize).Rows()
			defer rows.Close()
			if err != nil {
				fmt.Println("read diamond error: ", err)
				break
			}

			// for _, walletdiamond := range results {
			for rows.Next() {
				// var walletdiamond WalletDiamond
				// rows.Scan(&walletdiamond)
				var (
					id      int
					userId  string
					account string
					sn      string
					num     int64
				)
				err1 := rows.Scan(&id, &userId, &account, &sn, &num)
				if err1 != nil {
					fmt.Println("rows.scan() err1:", err1)
					break
				}
				account = strings.ToLower(account)

				key := userId + ":" + account

				if _, ok := WalletDiamondMap[key]; ok {
					diamondList := WalletDiamondMap[key]
					diamondVal := sn + ":" + strconv.FormatInt(num, 10)
					diamondList = append(diamondList, diamondVal)

					WalletDiamondMap[key] = diamondList
				} else {
					diamondValue := sn + ":" + strconv.FormatInt(num, 10)
					diamondValueList := make([]interface{}, 1)
					diamondValueList[0] = diamondValue

					WalletDiamondMap[key] = diamondValueList
				}

			}

			cursor += resultsize
		}
	}
}

func Readtickettomap() {
	/*
		{
			$uid:$name: [$sn:$num],
			$uid:$name: [$sn:$num],
			$uid:$name: [$sn:$num]
		}
	*/
	WalletTicketMap = make(map[string][]interface{})

	for i := 0; i < config.Config.Dbbak.Tablesplice; i++ {

		cursor := 0
		resultsize := 3000

		var count int
		models.DB.Table("newbank_walletticket_" + strconv.Itoa(i)).Model(&models.WalletDiamond{}).Count(&count)

		loopcount := count/resultsize + 1

		sql := `select id, user_id, account, sn, num from newbank_walletticket_` + strconv.Itoa(i) + ` order by id limit ?, ?`
		for i := 0; i < loopcount; i++ {
			rows, err := models.DB.Raw(sql, cursor, resultsize).Rows()
			defer rows.Close()
			if err != nil {
				fmt.Println("read diamond error: ", err)
				break
			}

			for rows.Next() {
				var (
					id      int
					userId  string
					account string
					sn      string
					num     int64
				)
				err1 := rows.Scan(&id, &userId, &account, &sn, &num)
				if err1 != nil {
					fmt.Println("rows.scan() err1:", err1)
					break
				}
				account = strings.ToLower(account)

				key := userId + ":" + account
				if _, ok := WalletTicketMap[key]; ok {
					ticketList := WalletTicketMap[key]
					ticketVal := sn + ":" + strconv.FormatInt(num, 10)
					ticketList = append(ticketList, ticketVal)

					WalletTicketMap[key] = ticketList
				} else {
					ticketValue := sn + ":" + strconv.FormatInt(num, 10)
					ticketValueList := make([]interface{}, 1)
					ticketValueList[0] = ticketValue

					WalletTicketMap[key] = ticketValueList
				}

			}

			cursor += resultsize
		}
	}
}

func ReadAccounttomap() {
	/*
		{
			$uid:$name:$currency : $balance
		}
	*/
	CoreAccountMap = make(map[string]int64)

	cursor := 0
	resultsize := 3000

	var count int
	models.DB.Table("core_account").Model(&models.CoreAccount{}).Count(&count)

	loopcount := count/resultsize + 1

	sql := `select id, user_id, name, currency, balance from newbank_coreaccount  order by id limit ?, ?`
	for i := 0; i < loopcount; i++ {

		rows, err := models.DB.Raw(sql, cursor, resultsize).Rows()
		defer rows.Close()
		if err != nil {
			fmt.Println("read diamond error: ", err)
			break
		}

		for rows.Next() {
			var (
				id       int
				userId   string
				name     string
				currency string
				balance  int64
			)
			err1 := rows.Scan(&id, &userId, &name, &currency, &balance)
			if err1 != nil {
				fmt.Println("rows.scan() err1:", err1)
				break
			}
			name = strings.ToLower(name)
			key := userId + ":" + name + ":" + currency

			if _, ok := CoreAccountMap[key]; ok {
				if CoreAccountMap[key] > 0 {
					accountBalance := CoreAccountMap[key]
					accountBalance = accountBalance + balance
					CoreAccountMap[key] = accountBalance
				} else {
					CoreAccountMap[key] = balance
				}
			} else {
				CoreAccountMap[key] = balance
			}
		}
		cursor += resultsize
	}

}

func WalletDiamondMapPlus(user_id string, account string, sn string, num int64) {

	key := user_id + ":" + account
	numminus := num
	if _, ok := WalletDiamondMap[key]; ok {
		diamondList := WalletDiamondMap[key]
		if diamondList != nil {

			// 看看有没有欠款
			delKey := -1
			for k, v := range diamondList {
				ssn := strings.Split(v.(string), ":")[0]
				snum := strings.Split(v.(string), ":")[1]
				dnumint, err := strconv.ParseInt(snum, 10, 64)
				if err != nil {
					panic(string("wrong format diamondmap : " + user_id + " " + account))
				}
				if dnumint < 0 {
					// 欠钱
					numminus = dnumint + numminus

					if numminus < 0 {
						// 还欠
						diamondList[k] = ssn + ":" + strconv.FormatInt(numminus, 10)
						fmt.Println("WalletDiamondMapPlus() key:", key, " value:", WalletDiamondMap[key])
						return
					} else if numminus == 0 {
						// 结清
						delKey = k
						if len(diamondList) > delKey+1 {
							diamondList = diamondList[delKey+1:]
							WalletDiamondMap[key] = diamondList
						} else {
							diamondList = nil
							WalletDiamondMap[key] = nil
						}
						return
					} else {
						diamondList[k] = ssn + ":" + strconv.FormatInt(numminus, 10)
						fmt.Println("WalletDiamondMapPlus() key:", key, " value:", WalletDiamondMap[key])
						return
					}
				}
			}

			if sn == "N0" {
				dicmondVal := diamondList[0]
				if strings.HasPrefix(dicmondVal.(string), "N0:") {
					snum := strings.Split(dicmondVal.(string), ":")[1]
					tnumint, err := strconv.ParseInt(snum, 10, 64)
					if err != nil {
						panic(string("Error: wrong format diamondmap : " + user_id + " (" + account + ")"))
					}
					dicmondVal = "N0" + ":" + strconv.FormatInt(num+tnumint, 10)
					diamondList[0] = dicmondVal
				} else {
					dicmondVal = "N0:" + strconv.FormatInt(num, 10)
					diamondList = append(diamondList, dicmondVal)
					copy(diamondList[1:], diamondList[0:])
					diamondList[0] = dicmondVal
				}

				WalletDiamondMap[key] = diamondList
			} else {
				diamondVal := sn + ":" + strconv.FormatInt(numminus, 10)
				lastOne := diamondList[len(diamondList)-1]
				ssn := strings.Split(lastOne.(string), ":")[0]
				snum := strings.Split(lastOne.(string), ":")[1]
				if ssn == sn {
					snnum, _ := strconv.ParseInt(snum, 10, 64)
					nnum := snnum + numminus
					diamondList[len(diamondList)-1] = sn + ":" + strconv.FormatInt(nnum, 10)
				} else {
					diamondList = append(diamondList, diamondVal)
				}

				WalletDiamondMap[key] = diamondList
			}
		} else {
			diamondValue := sn + ":" + strconv.FormatInt(num, 10)
			diamondValueList := make([]interface{}, 1)
			diamondValueList[0] = diamondValue

			WalletDiamondMap[key] = diamondValueList
		}

	} else {
		diamondValue := sn + ":" + strconv.FormatInt(num, 10)
		diamondValueList := make([]interface{}, 1)
		diamondValueList[0] = diamondValue

		WalletDiamondMap[key] = diamondValueList
	}

	fmt.Println("WalletDiamondMapPlus() key:", key, " value:", WalletDiamondMap[key])
}

func WalletTicketMapPlus(user_id string, account string, sn string, num int64) {
	fmt.Println("WalletTicketMapPlus() user:", user_id, " account:", account, " sn:", sn, " num:", num)
	key := user_id + ":" + account
	if _, ok := WalletTicketMap[key]; ok {
		ticketList := WalletTicketMap[key]
		if ticketList != nil {
			if sn == "N0" {
				ticketVal := ticketList[0]
				if strings.HasPrefix(ticketVal.(string), "N0:") {
					snum := strings.Split(ticketVal.(string), ":")[1]
					tnumint, err := strconv.ParseInt(snum, 10, 64)
					if err != nil {
						panic(string("Error: wrong format ticketmap : " + user_id + " (" + account + ")"))
					}
					ticketVal = "N0" + ":" + strconv.FormatInt(num+tnumint, 10)
					ticketList[0] = ticketVal
				} else {
					ticketVal = "N0:" + strconv.FormatInt(num, 10)
					ticketList = append(ticketList, ticketVal)
					copy(ticketList[1:], ticketList[0:])
					ticketList[0] = ticketVal
				}
				WalletTicketMap[key] = ticketList
			} else {
				ticketVal := sn + ":" + strconv.FormatInt(num, 10)

				lastOne := ticketList[len(ticketList)-1]
				ssn := strings.Split(lastOne.(string), ":")[0]
				snum := strings.Split(lastOne.(string), ":")[1]
				if ssn == sn {
					snnum, _ := strconv.ParseInt(snum, 10, 64)
					nnum := snnum + num
					ticketList[len(ticketList)-1] = sn + ":" + strconv.FormatInt(nnum, 10)
				} else {
					ticketList = append(ticketList, ticketVal)
				}
				WalletTicketMap[key] = ticketList
			}
		} else {
			ticketValue := sn + ":" + strconv.FormatInt(num, 10)
			ticketValueList := make([]interface{}, 1)
			ticketValueList[0] = ticketValue

			WalletTicketMap[key] = ticketValueList
		}

	} else {
		ticketValue := sn + ":" + strconv.FormatInt(num, 10)
		ticketValueList := make([]interface{}, 1)
		ticketValueList[0] = ticketValue

		WalletTicketMap[key] = ticketValueList
	}

	fmt.Println("WalletTicketMapPlus() key:", key, " value:", WalletTicketMap[key])
}

func CoreAccountMapPlus(user_id string, account string, currency string, balance int64) {
	key := user_id + ":" + account + ":" + currency
	fmt.Println("CoreAccountMapPlus() start key:", key, "  value:", CoreAccountMap[key])
	if _, ok := CoreAccountMap[key]; ok {
		accountBalance := CoreAccountMap[key]
		accountBalance = accountBalance + balance
		CoreAccountMap[key] = accountBalance
	} else {
		CoreAccountMap[key] = balance
	}
	fmt.Println("CoreAccountMapPlus() end key:", key, "  value:", CoreAccountMap[key])
}

func WalletDiamondMapMinus(user_id string, account string, sn string, num int64) (map[string]int64, error) {
	var snmaps map[string]int64
	snmaps = make(map[string]int64)

	key := user_id + ":" + account
	numminus := num
	if _, ok := WalletDiamondMap[key]; ok {
		diamondList := WalletDiamondMap[key]
		if diamondList != nil {

			fmt.Println("WalletDiamondMapMinus() start key:", key, "  value:", diamondList)
			delKey := -1
			for k, v := range diamondList {
				ssn := strings.Split(v.(string), ":")[0]
				snum := strings.Split(v.(string), ":")[1]
				dnumint, err := strconv.ParseInt(snum, 10, 64)
				if err != nil {
					panic(string("wrong format diamondmap : " + user_id + " " + account))
				}
				if dnumint > numminus {
					diamondList[k] = ssn + ":" + strconv.FormatInt(dnumint-numminus, 10)
					snmaps[ssn] = snmaps[ssn] + numminus
					numminus = 0
					break
				} else if dnumint <= numminus {

					numminus = numminus - dnumint
					snmaps[ssn] = snmaps[ssn] + dnumint
					delKey = k
					if numminus == 0 {
						break
					} else {
						continue
					}
				}
			}
			// 有需要删除的key
			if delKey >= 0 {
				if len(diamondList) > delKey+1 {
					//diamondList = append(diamondList[:delKey], diamondList[delKey+1:]...)
					diamondList = diamondList[delKey+1:]
					WalletDiamondMap[key] = diamondList
				} else {
					diamondList = nil
					WalletDiamondMap[key] = nil
				}
			}
			// 实际上如果减完还有值。 说明没减够。 就欠着。
			if numminus > 0 {

				diamondValue := "N0" + ":" + strconv.FormatInt(-numminus, 10)
				diamondValueList := make([]interface{}, 1)
				diamondValueList[0] = diamondValue

				WalletDiamondMap[key] = diamondValueList

				snmaps["N0"] = snmaps["N0"] + numminus

				//return snmaps, errors.New("diamond account " + user_id + " " + account + " not enough " + strconv.FormatInt(num, 10) + "balance.")
			}
		} else {
			diamondValue := "N0" + ":" + strconv.FormatInt(-numminus, 10)
			diamondValueList := make([]interface{}, 1)
			diamondValueList[0] = diamondValue

			WalletDiamondMap[key] = diamondValueList

			snmaps["N0"] = snmaps["N0"] + numminus
		}

		fmt.Println("WalletDiamondMapMinus() end key:", key, "  value:", WalletDiamondMap[key])

	} else {
		// 账户没钱？就先欠着
		diamondValue := "N0" + ":" + strconv.FormatInt(-numminus, 10)
		diamondValueList := make([]interface{}, 1)
		diamondValueList[0] = diamondValue

		WalletDiamondMap[key] = diamondValueList

		snmaps["N0"] = snmaps["N0"] + numminus

		fmt.Println("WalletDiamondMapMinus() end key:", key, "  value:", diamondValueList)

	}
	return snmaps, nil
}

func WalletTicketMapMinus(user_id string, account string, sn string, num int64) (map[string]int64, error) {
	var snmaps map[string]int64
	snmaps = make(map[string]int64)

	key := user_id + ":" + account
	numminus := num
	if _, ok := WalletTicketMap[key]; ok {
		ticketList := WalletTicketMap[key]
		if ticketList != nil {

			delKey := -1
			for k, v := range ticketList {
				ssn := strings.Split(v.(string), ":")[0]
				snum := strings.Split(v.(string), ":")[1]
				tnumint, err := strconv.ParseInt(snum, 10, 64)
				if err != nil {
					panic(string("wrong format ticketmap : " + user_id + " " + account))
				}
				if tnumint > numminus {
					ticketList[k] = ssn + ":" + strconv.FormatInt(tnumint-numminus, 10)
					snmaps[ssn] = snmaps[ssn] + numminus
					numminus = 0
					break
				} else if tnumint <= numminus {

					numminus = numminus - tnumint
					snmaps[ssn] = snmaps[ssn] + tnumint
					delKey = k
					if numminus == 0 {
						break
					} else {
						continue
					}
				}
			}

			if delKey >= 0 {
				if len(ticketList) >= delKey+1 {
					// ticketList = append(ticketList[:delKey], ticketList[delKey+1:]...)
					ticketList = ticketList[delKey+1:]
					WalletTicketMap[key] = ticketList
				} else {
					ticketList = nil
					WalletTicketMap[key] = nil
				}
			}

			if numminus > 0 {
				ticketValue := "N0" + ":" + strconv.FormatInt(-numminus, 10)
				ticketValueList := make([]interface{}, 1)
				ticketValueList[0] = ticketValue

				WalletTicketMap[key] = ticketValueList

				snmaps["N0"] = snmaps["N0"] + numminus
				//return snmaps, errors.New("ticket account " + user_id + " " + account + " not enough " + strconv.FormatInt(num, 10) + "balance.")
			}
		} else {
			ticketValue := "N0" + ":" + strconv.FormatInt(-numminus, 10)
			ticketValueList := make([]interface{}, 1)
			ticketValueList[0] = ticketValue

			WalletTicketMap[key] = ticketValueList

			snmaps["N0"] = snmaps["N0"] + numminus
		}
	} else {
		ticketValue := "N0" + ":" + strconv.FormatInt(-numminus, 10)
		ticketValueList := make([]interface{}, 1)
		ticketValueList[0] = ticketValue

		WalletTicketMap[key] = ticketValueList

		snmaps["N0"] = snmaps["N0"] + numminus
	}
	return snmaps, nil
}

func CoreAccountMapMinus(user_id string, account string, currency string, balance int64) error {
	key := user_id + ":" + account + ":" + currency
	fmt.Println("CoreAccountMapMinus() start key:", key, "  value:", CoreAccountMap[key])
	if _, ok := CoreAccountMap[key]; ok {
		if CoreAccountMap[key] > 0 {
			if CoreAccountMap[key] >= balance {
				accountBalance := CoreAccountMap[key]
				accountBalance = accountBalance - balance
				CoreAccountMap[key] = accountBalance

			} else {
				// 没有这么多币， 就欠着
				accountBalance := CoreAccountMap[key]
				accountBalance = accountBalance - balance
				CoreAccountMap[key] = accountBalance
			}
		} else {
			// 没币就欠着
			accountBalance := CoreAccountMap[key]
			accountBalance = -balance + accountBalance
			CoreAccountMap[key] = accountBalance

		}
	} else {
		accountBalance := CoreAccountMap[key]
		accountBalance = -balance
		CoreAccountMap[key] = accountBalance
	}

	fmt.Println("CoreAccountMapMinus() end key:", key, "  value:", CoreAccountMap[key])
	return nil
}

func SaveWalletDiamondToDB() error {
	CreateTable("walletdiamond", "_tmp")

	wds := make([]*models.WalletDiamond, 0)
	for k, v := range WalletDiamondMap {
		if wds == nil {
			wds = make([]*models.WalletDiamond, 0)
		}

		uid := strings.Split(k, ":")[0]
		account := strings.Split(k, ":")[1]

		for _, v1 := range v {
			if v1 == nil {
				continue
			}
			sn := strings.Split(v1.(string), ":")[0]
			num := strings.Split(v1.(string), ":")[1]
			inum, _ := strconv.ParseInt(num, 10, 64)
			walletdiamond := &models.WalletDiamond{UserId: uid, Account: account, Sn: sn, Num: inum}
			wds = append(wds, walletdiamond)
		}

		if len(wds) > 6000 {
			insertDiamondDB(wds, "_tmp")
			wds = nil
		}
	}

	if len(wds) > 0 {
		insertDiamondDB(wds, "_tmp")
	}

	ReplaceTable("walletdiamond", "_tmp")

	return nil
}

func SaveWalletTicketToDB() error {
	CreateTable("walletticket", "_tmp")

	wds := make([]*models.WalletTicket, 0)
	for k, v := range WalletTicketMap {
		if wds == nil {
			wds = make([]*models.WalletTicket, 0)
		}

		uid := strings.Split(k, ":")[0]
		account := strings.Split(k, ":")[1]

		for _, v1 := range v {
			if v1 == nil {
				continue
			}
			sn := strings.Split(v1.(string), ":")[0]
			num := strings.Split(v1.(string), ":")[1]
			inum, _ := strconv.ParseInt(num, 10, 64)
			walletticket := &models.WalletTicket{UserId: uid, Account: account, Sn: sn, Num: inum}
			wds = append(wds, walletticket)
		}

		if len(wds) > 6000 {
			insertTicketDB(wds, "_tmp")
			wds = nil
		}
	}

	if len(wds) > 0 {
		insertTicketDB(wds, "_tmp")
	}

	ReplaceTable("walletticket", "_tmp")

	return nil
}

func SaveCoreAccountToDB() error {
	CreateCoreAccountTable("_tmp")

	cas := make([]*models.CoreAccount, 0)
	for k, v := range CoreAccountMap {
		if cas == nil {
			cas = make([]*models.CoreAccount, 0)
		}

		uid := strings.Split(k, ":")[0]
		account := strings.Split(k, ":")[1]
		currency := strings.Split(k, ":")[2]

		coreaccount := &models.CoreAccount{UserId: uid, Name: account, Currency: currency, Balance: v}
		cas = append(cas, coreaccount)

		if len(cas) > 6000 {
			insertAccountDB(cas, "_tmp")
			cas = nil
		}
	}

	if len(cas) > 0 {
		insertAccountDB(cas, "_tmp")
	}

	ReplaceCoreAccountTable("_tmp")

	return nil
}

func CreateTable(tablename string, tablePrefix string) {

	d, err := ioutil.ReadFile("models/mkg/" + tablename + ".sql")
	if err != nil {
		fmt.Print(err)
	}

	sqldiamond := string(d)

	models.DB.Exec(strings.Replace("drop table if exists `newbank_"+tablename+"_%s`;", "%s", "e1"+tablePrefix, 1))
	sql_e1 := strings.Replace(sqldiamond, "%s", "e1"+tablePrefix, 1)
	// fmt.Println("create tbl :", sql_e1)
	models.DB.Exec(sql_e1)

	var sql_exec string
	for i := 0; i < config.Config.Dbbak.Tablesplice; i++ {
		models.DB.Exec(strings.Replace("drop table if exists `newbank_"+tablename+"_%s`;", "%s", strconv.Itoa(i)+tablePrefix, 1))
		sql_exec = strings.Replace(sqldiamond, "%s", strconv.Itoa(i)+tablePrefix, 1)
		// fmt.Println("create tbl :", sql_exec)
		models.DB.Exec(sql_exec)
	}
}

func CreateCoreAccountTable(tablePrefix string) {

	d, err := ioutil.ReadFile("models/mkg/coreaccount.sql")
	if err != nil {
		fmt.Print(err)
	}

	sqldiamond := string(d)

	models.DB.Exec("drop table if exists `newbank_coreaccount" + tablePrefix + "`;")
	sql_e1 := strings.Replace(sqldiamond, "%s", tablePrefix, 1)
	// fmt.Println("create tbl :", sql_e1)
	models.DB.Exec(sql_e1)

}

func ReplaceTable(tablename string, tablePrefix string) {

	models.DB.Exec("drop table if exists `newbank_" + tablename + "_e1`;")
	sql_e1 := "rename table `newbank_" + tablename + "_e1" + tablePrefix + "` to `newbank_" + tablename + "_e1`;"
	// fmt.Println("rename tbl :", sql_e1)
	models.DB.Exec(sql_e1)

	var sql_exec string
	for i := 0; i < config.Config.Dbbak.Tablesplice; i++ {
		sql_exec = "drop table if exists `newbank_" + tablename + "_" + strconv.Itoa(i) + "`;"
		// fmt.Println("drop tbl :", sql_exec)
		models.DB.Exec(sql_exec)
		sql_exec = "rename table `newbank_" + tablename + "_" + strconv.Itoa(i) + tablePrefix + "` to `newbank_" + tablename + "_" + strconv.Itoa(i) + "`;"
		// fmt.Println("rename tbl :", sql_exec)
		models.DB.Exec(sql_exec)
	}
}

func ReplaceCoreAccountTable(tablePrefix string) {

	models.DB.Exec("drop table if exists `newbank_coreaccount`;")
	sql_e1 := "rename table `newbank_coreaccount" + tablePrefix + "` to `newbank_coreaccount`;"
	// fmt.Println("rename tbl :", sql_e1)
	models.DB.Exec(sql_e1)

}

func insertDiamondDB(kps []*models.WalletDiamond, tablePrefix string) error {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println("insertDiamondDB() err: ", err, "!!!!!!!!!!!!!!!!!!!!")
			panic(err)
		}
	}()

	//构建SQL语句
	sql_e1 := `insert into newbank_walletdiamond_e1` + tablePrefix + `(user_id, sn, account, num, created_at) values`

	sql_diamond := make([]string, config.Config.Dbbak.Tablesplice)

	for i := 0; i < len(sql_diamond); i++ {
		sql_diamond[i] = `insert into newbank_walletdiamond_` + strconv.Itoa(i) + tablePrefix + `(user_id, sn, account, num, created_at) values`
	}

	haserr := false

	for _, kp := range kps {
		personValue := `("` + kp.UserId + `","` + kp.Sn + `","` + kp.Account + `",` + strconv.FormatInt(kp.Num, 10) + `,"` + time.Now().Format("2006-01-02 03:04:05.999999999") + `"),`
		UserId, err := strconv.ParseInt(kp.UserId, 10, 64)
		if err != nil {
			haserr = true
			sql_e1 += personValue
		} else {
			tableIndex := int(UserId) % config.Config.Dbbak.Tablesplice
			sql_diamond[tableIndex] += personValue
		}
	}

	if haserr {
		sql_e1 = sql_e1[:len(sql_e1)-1] + ";"
		models.DB.Exec(sql_e1)
	}

	for i := 0; i < len(sql_diamond); i++ {
		sql_diamond[i] = sql_diamond[i][:len(sql_diamond[i])-1] + ";"
		if len(sql_diamond[i]) <= 90 {
			continue
		}
		err1 := models.DB.Exec(sql_diamond[i]).Error
		if err1 != nil {
			fmt.Println("insertDiamondDB() err: ", err1)
			panic(err1)
		}
	}

	kps = kps[0:0]

	return nil
}

func insertTicketDB(kps []*models.WalletTicket, tablePrefix string) error {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println("insertTicketDB() err: ", err, "!!!!!!!!!!!!!!!!!!!!")
			panic(err)
		}
	}()

	//构建SQL语句
	sql_e1 := `insert into newbank_walletticket_e1` + tablePrefix + `(user_id, sn, account, num, created_at) values`

	sql_ticket := make([]string, config.Config.Dbbak.Tablesplice)

	for i := 0; i < len(sql_ticket); i++ {
		sql_ticket[i] = `insert into newbank_walletticket_` + strconv.Itoa(i) + tablePrefix + `(user_id, sn, account, num, created_at) values`
	}

	haserr := false

	for _, kp := range kps {
		personValue := `("` + kp.UserId + `","` + kp.Sn + `","` + kp.Account + `",` + strconv.FormatInt(kp.Num, 10) + `,"` + time.Now().Format("2006-01-02 03:04:05.999999999") + `"),`
		UserId, err := strconv.ParseInt(kp.UserId, 10, 64)
		if err != nil {
			haserr = true
			sql_e1 += personValue
		} else {
			tableIndex := int(UserId) % config.Config.Dbbak.Tablesplice
			sql_ticket[tableIndex] += personValue
		}
	}

	if haserr {
		sql_e1 = sql_e1[:len(sql_e1)-1] + ";"
		models.DB.Exec(sql_e1)
	}

	for i := 0; i < len(sql_ticket); i++ {
		sql_ticket[i] = sql_ticket[i][:len(sql_ticket[i])-1] + ";"
		if len(sql_ticket[i]) <= 90 {
			continue
		}

		err1 := models.DB.Exec(sql_ticket[i]).Error
		if err1 != nil {
			fmt.Println("insertTicketDB() err: ", err1)
			panic(err1)
		}
	}

	kps = kps[0:0]

	return nil
}

func insertAccountDB(cas []*models.CoreAccount, tablePrefix string) error {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println("insertAccountDB() err: ", err, "!!!!!!!!!!!!!!!!!!!!")
			panic(err)
		}
	}()

	tx := models.DB.Begin()

	//构建SQL语句
	sql_ := `insert into newbank_coreaccount` + tablePrefix + `(user_id, name, currency, balance, created_at) values`

	for _, ca := range cas {
		personValue := `("` + ca.UserId + `","` + ca.Name + `","` + ca.Currency + `",` + strconv.FormatInt(ca.Balance, 10) + `,"` + time.Now().Format("2006-01-02 03:04:05.999999999") + `"),`
		sql_ += personValue
	}

	sql_ = sql_[:len(sql_)-1] + ";"
	models.DB.Exec(sql_)

	tx.Commit()

	cas = cas[0:0]

	return nil
}

func Snapshoot(datestr string) {
	exportDB("newbank_coreaccount", "", "newbank_coreaccount_"+datestr)

	exportDB("newbank_walletticket_e1", "", "newbank_walletticket_e1_"+datestr)

	sql_ticket := make([]string, config.Config.Dbbak.Tablesplice)
	for i := 0; i < len(sql_ticket); i++ {
		exportDB("newbank_walletticket_"+strconv.Itoa(i), "", "newbank_walletticket_"+strconv.Itoa(i)+"_"+datestr)
		exportDB("newbank_walletdiamond_"+strconv.Itoa(i), "", "newbank_walletdiamond_"+strconv.Itoa(i)+"_"+datestr)
	}
}

func exportDB(tablename string, wheresql string, snapshootname string) error {
	argv := []string{"--complete-insert", "--skip-comments", "--compact", "--add-drop-table", "-h" + config.Config.Mysql.Host, "-u" + config.Config.Mysql.User, "-p" + config.Config.Mysql.Password, config.Config.Mysql.Db, tablename}
	if wheresql != "" {
		argv = append(argv, `--where"`+wheresql+`"`)
	}
	fmt.Println("command: ", argv)
	cmd := exec.Command("mysqldump", argv...)
	f, err := os.OpenFile(config.Config.Dbbak.Filepath+"snapshoots/"+snapshootname+".sql", os.O_CREATE|os.O_RDWR, os.ModePerm|os.ModeTemporary)
	if err != nil {
		fmt.Println("打开sql文件失败, err:", err)
		return err
	}
	defer f.Close()
	cmd.Stdout = f
	cmd.Stderr = os.Stderr
	cmd.Start()
	cmd.Run()
	cmd.Wait()
	return nil
}
