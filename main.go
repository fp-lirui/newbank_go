package main

import (
	"encoding/json"
	"flag"
	"fmt"
	_ "fmt"
	jsoniter "github.com/json-iterator/go"
	"github.com/json-iterator/go/extra"
	"st/models"
	"st/utils"
	"time"

	log "github.com/sirupsen/logrus"
	"st/service"
	"strings"
)

type TransData struct {
	Type             string
	External_id      string
	Debit_user       string
	Debit_account    string
	Credit_user      string
	Credit_account   string
	Base_currency    string
	Counter_currency string
	Consume_type     string
	Amount           string
	Exchange_rate    string
	Region           string
	Transaction_id   string
	User_id          string
	Currency         string
	Transactions     []TransData
}

type BulkTrans struct {
	TransData    []TransData
	Type         string
	Consume_type string
	External_id  string
}

type Bundle struct {
	DIAMOND string
	TICKET  string
}

type AAA interface {
	AAAtest()
	BBBtest()
}

type AAAMethod struct {
	AAA
}

type BBBMethod struct {
	AAA
}

func (b *BBBMethod) AAAtest() {
	println("BBBmethod -> AAAtest()")
}

func (b *BBBMethod) BBBtest() {
	println("BBBMethod -> BBBtest()")
}

func (a *AAAMethod) AAAtest() {
	println("AAAMethod -> AAAtest()")
}

func (a *AAAMethod) BBBtest() {
	println("AAAMethod -> BBBtest()")
}

func maintest() {

	userRisk, _ := service.AddUserRisks("423", 1, 0, 0)
	models.UpdateBankUserRisk(userRisk, 1, 0, 0)

	return

	a11 := "appleiap.490000702259169"
	b11 := strings.Replace(a11, "appleiap.", "", 1)
	fmt.Println(b11)
	return

	error := utils.Sendmail("rui.li@nextentertain.com:lee-ray@hotmail.com", "test444333", "lee-ray@hotmail.com", "test body", "text")
	fmt.Printf("error: %v", error)

	return

	service.SyncGoogleVoidedPurchasesTable()

	return

	fmt.Printf("unix: %v\n", time.Now().Unix() - 48*60*60)
	fmt.Printf("unixnano: %v\n", time.Now().UnixNano()/1e6 - 48*60*60*1000)
	return

	//models.UpdateGoogleVoidedPurchasesStatus(`GPA.3344-8188-7633-17207`, 2)
	//models.UpdateDepositLogRefunded(200000000)
	//
	//return
	voidedpurchase_jp, errorjp := service.OrderCheckerWithCredentials(false, "chat.meme.perky", "/Users/lirui/WorkSpaces/Golang/newbank_go/config/api-6627288011901432293-40929-fb48ec56c9e5.json")
	if errorjp != nil {
		if voidedpurchase_jp != nil {

		}
	}
	voidedpurchase_in, errorin := service.OrderCheckerWithCredentials(false, "chat.meme.inke", "/Users/lirui/WorkSpaces/Golang/newbank_go/config/api-8640825876244245346-296145-84cbefccaf9b.json")
	if errorin != nil {
		if voidedpurchase_in != nil {

		}
	}

	return

	a1 := `appleiap.240000788470520`

	b1 := a1[9:]

	println(a1)
	println(b1)

	return

	var a AAA

	a = &AAAMethod{}
	a.AAAtest()
	a.BBBtest()

	var b AAA
	b = &BBBMethod{}
	b.AAAtest()
	b.BBBtest()

	return
	response := `{\"code\": 0, \"reason\": \"Success\", \"body\": {\"external_id\": \"googleplayiab.android.test.purchased.1594116293.8802586_refund\", \"trans_id\": \"dc6d66b0672f93576373039cb35e0f\", \"affected_accounts\": [{\"user_id\": \"1612030\", \"balance_delta\": \"-35980\", \"account\": \"default\", \"currency\": \"DIAMOND\"}, {\"user_id\": \"1612030\", \"balance_delta\": \"35980\", \"account\": \"refund_locked\", \"currency\": \"DIAMOND\"}]}}`


	response = strings.ReplaceAll(response, `\`, "")
	response = strings.ReplaceAll(response, "\n", "")
	response = strings.ReplaceAll(response, "\"{", "{")
	response = strings.ReplaceAll(response, "}\"", "}")
	//response = strings.ReplaceAll(response, " ", "")

	var respJson service.TransactionResponse
	extra.RegisterFuzzyDecoders()
	var json_iterator = jsoniter.ConfigFastest
	json_iterator.Unmarshal([]byte(response), &respJson)
	code := respJson.Code
	reason := respJson.Reason
	bodyJson := respJson.Body
	refunded_external_id := bodyJson.ExternalId
	refunded_trans_id := bodyJson.TransId

	log.WithFields(log.Fields{"response":response, "code":code, "reason":reason, "body":bodyJson, "refunded_external_id":refunded_external_id, "refunded_trans_id": refunded_trans_id}).Info("bank response.")

	return
	jsonstr1 := `[{"debit_user": "6090", "amount": "1", "debit_account": "default", "credit_account": "", "currency": "TICKET", "type": "transfer", "credit_user": "__MANUALWITHDRAW"},{"debit_user": "6090", "amount": "1", "debit_account": "default", "credit_account": "", "currency": "TICKET", "type": "transfer", "credit_user": "__MANUALWITHDRAW"}]`
	jsonstr2 := `{"consume_type": "41", "type": "bulk", "transactions": [{"amount": "101", "currency": "DIAMOND", "credit_account": "lucky_gift", "debit_user": "1189451", "credit_user": "0", "type": "transfer", "debit_account": "default"}, {"base_currency": "DIAMOND", "amount": "50", "credit_account": "default", "counter_currency": "TICKET", "debit_user": "0", "credit_user": "1240841", "type": "gift", "debit_account": "lucky_gift"}], "external_id": "B79161B6-F696-4557-AE20-E5EF4EBA42AA"}`

	jsondata1 := []byte(jsonstr1)
	jsondata2 := []byte(jsonstr2)

	var transdata1 []TransData
	var transdata2 TransData

	err1 := json.Unmarshal(jsondata1, &transdata1)
	if err1 != nil {
		fmt.Println("json.unmarshal() bulkjson error1:", err1)
	}

	err2 := json.Unmarshal(jsondata2, &transdata2)
	if err2 != nil {
		fmt.Println("json.unmarshal() bulkjson error2:", err2)
	}
	// fmt.Println("bulkdata2:", bulkdata2)
	fmt.Println("transdata1:", transdata1)
	fmt.Println("transdata1 len():", len(transdata1))
	fmt.Println("transdata2:", transdata2)

	// for _, transjson := range bulkdata2 {
	// 	var transdata TransData
	// 	mapstructure.Decode(transjson, &transdata)
	// 	fmt.Println("transdata:", transdata)
	// }

	// err2 := json.Unmarshal(jsondata2, &transdata1)
	// if err2 != nil {
	// 	fmt.Println("json.unmarshal() transjson error:", err2)
	// }

	// fmt.Println("transdata1:", transdata1)
	// /fmt.Println(strings.Replace(strings.Replace("rename table `newbank_walletdiamond_%ts` to `newbank_walletdiamond_%s`;", "%ts", "e1"+"tablePrefix", 1), "%s", "e1", 1))

	return

	service.Readdiamondtomap()
	fmt.Println(service.WalletDiamondMap)
	service.WalletDiamondMapPlus("125", "game", "s1", 10000)
	fmt.Println(service.WalletDiamondMap)
	service.WalletDiamondMapMinus("125", "game", "s1", 1)
	fmt.Println(service.WalletDiamondMap)
	service.WalletDiamondMapPlus("125", "game", "s1", 10000)
	fmt.Println(service.WalletDiamondMap)
	service.WalletDiamondMapPlus("125", "game", "s1", 10000)
	fmt.Println(service.WalletDiamondMap)
	service.WalletDiamondMapMinus("125", "game", "s1", 18000)
	fmt.Println(service.WalletDiamondMap)

	service.SaveWalletDiamondToDB()
	//models.AddWalletDiamond("111", "sss", "sss", 111)
	//models.UpdateWalletDataConfigCursor(11)
	//models.UpdateCoreAccountBalancePlus("11", "default", "diamond", 111)

	// walletDiamonds, err1 := models.FindUserWalletDiamond("11111", "default")
	// if err1 != nil {
	// 	fmt.Println("walletdiamond error:", err1)
	// }
	// fmt.Println("walletdiamond:", walletDiamonds)

}

var (
	refundcheck    bool
	refundinit     bool
	checkout       bool
	test           bool
	sync           bool
	startdate      string
	enddate        string
)

func init() {
	flag.BoolVar(&test, "test", false, "for test.")
	flag.BoolVar(&refundcheck, "refundcheck", false, "receipt checker.")
	flag.BoolVar(&refundinit, "refundinit", false, "refund init.")
	flag.BoolVar(&checkout, "checkout", false, "checkout statements")
	flag.BoolVar(&sync, "sync", false, "sync mysql to redshift.")
	flag.StringVar(&startdate, "startdate", "", "start of day, format with yyyy-mm-dd.")
	flag.StringVar(&enddate, "enddate", "", "end of day, format with yyyy-mm-dd.")
}

func main() {

	flag.Parse()
	models.Setup()

	if test {
		maintest()
		return
	}

	if checkout {
		service.CheckoutPayment(startdate, enddate)
		return
	}

	if refundinit {
		voidedpurchase_jp, errorjp := service.OrderCheckerWithCredentials(true, "chat.meme.perky", "./config/api-6627288011901432293-40929-fb48ec56c9e5.json")
		if errorjp != nil {
			if voidedpurchase_jp != nil {

			}
		}
		voidedpurchase_in, errorin := service.OrderCheckerWithCredentials(true, "chat.meme.inke", "./config/api-8640825876244245346-296145-84cbefccaf9b.json")
		if errorin != nil {
			if voidedpurchase_in != nil {

			}
		}
	}

	if refundcheck {
		service.OrderCheckerWithCredentials(false, "chat.meme.perky", "./config/api-6627288011901432293-40929-fb48ec56c9e5.json")
		service.OrderCheckerWithCredentials(false, "chat.meme.inke", "./config/api-8640825876244245346-296145-84cbefccaf9b.json")

		service.RefundOrderIab()
		service.RefundOrderIap()
	}

	if sync {
		service.SyncGoogleVoidedPurchasesTable()
	}

}

