module st

go 1.14

require (
	cloud.google.com/go/storage v1.10.0
	github.com/gin-gonic/gin v1.6.3
	github.com/goinggo/mapstructure v0.0.0-20140717182941-194205d9b4a9
	github.com/gomodule/redigo v1.8.2
	github.com/jinzhu/gorm v1.9.14
	github.com/json-iterator/go v1.1.9
	github.com/lib/pq v1.1.1
	github.com/sirupsen/logrus v1.6.0
	github.com/stretchr/testify v1.6.1
	github.com/tealeg/xlsx v1.0.5
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
	golang.org/x/text v0.3.3
	google.golang.org/api v0.30.0
	google.golang.org/appengine v1.6.6 // indirect
)
