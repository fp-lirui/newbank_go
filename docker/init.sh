#!/bin/bash -e

export CMD="$1"

: ${DEPLOYMENT_KEY?"You must set environment 'DEPLOYMENT_KEY' to specific the git deployment key."}
: ${REPO_SSH_URL?"You must set environment 'REPO_SSH_URL' to specific the repo URL."}
: ${DB_HOST?"You must set environment 'DB_HOST' to specific the hostname of MySQL service."}
: ${DB_NAME?"You must set environment 'DB_NAME' to specific the database name."}
: ${DB_PORT?"You must set environment 'DB_PORT' to specific the mysql port."}
: ${DB_USER?"You must set environment 'DB_USER' to specific the database user."}
: ${DB_PASSWORD?"You must set environment 'DB_PASSWORD' to specific the database password."}

mkdir /root/.ssh/ -p
echo "$DEPLOYMENT_KEY" > /root/.ssh/id_rsa
chmod 600 /root/.ssh/id_rsa
echo "StrictHostKeyChecking no" > /root/.ssh/config

if [ ! -f /mnt/docker/entry.sh ];
then
    git clone $REPO_SSH_URL /mnt
else
    cd /mnt && git pull
fi

echo "Initializing..."
/mnt/docker/entry.sh